FROM openjdk:8-alpine

RUN apk add --no-cache bash

RUN addgroup -g 1001 -S pitre && adduser -u 1001 -S pitre -G pitre
RUN chown pitre:pitre /srv
USER pitre


WORKDIR /srv

COPY ./build .
COPY ./csv2properties.sh .
#COPY ./properties ./properties

COPY docker-entrypoint.sh /
COPY Dockerfile /

CMD ["/docker-entrypoint.sh"]

EXPOSE 8080
