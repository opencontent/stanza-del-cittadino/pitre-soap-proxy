#!/usr/bin/env sh
echo

echo "==> Checking files and parameters..."
echo 
if [[ ! -f P3WSJavaClient.jar ]]; then
  echo "Missing jar file"
  exit 1
fi

if [[ ! -d ./certificate ]]; then
  echo "Missing certificates directory"
  exit 2
fi

echo "==> Creating properties files..."
echo 
./csv2properties.sh
if [[ $? -gt 0 ]]; then
  echo "Error occurred while generating properties files"
  exit 3
fi

if [[ ! -d ./properties ]]; then
  echo "Missing properties directory"
  exit 4
fi


echo
echo "==> Starting Jetty app..." 
echo 
exec java -jar P3WSJavaClient.jar --create-startd=jmx,stats

echo "==> Terminated gracefully"
echo
