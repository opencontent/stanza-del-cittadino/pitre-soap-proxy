# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Generated by [`auto-changelog`](https://github.com/CookPete/auto-changelog).

## [1.3.0](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/compare/1.2.0...1.3.0)

### Commits

- Updated trustore for test server [`a9e2729`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/a9e272924680ee0cc5ca08c075f19d110043f332)

## [1.2.0](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/compare/1.1.13-rc.0...1.2.0) - 2024-06-06

### Commits

- Release 1.2.0 [`65ff44a`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/65ff44ac2da9b00ad2c0bae92f27ccde6235a773)
- Restore transmission type from T to S [`4cd169b`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/4cd169bc7d3a07d616bfc332976392f82c27c563)
- Updated Pitre CA [`e1316a5`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/e1316a50c0400be08e9b9de0f7a7f41e5e9c9a51)

## [1.1.13-rc.0](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/compare/1.1.12...1.1.13-rc.0) - 2024-05-22

### Merged

- Resolve "Modifica tipologia di trasmissione" [`#5`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/merge_requests/5)

### Fixed

- Merge branch '1-modifica-tipologia-di-trasmissione' into 'main' [`#1`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/issues/1)

### Commits

- Release 1.1.13-rc.0 [`4f2b643`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/4f2b643a26bf8da85f2575f7732bbbccfe83d0f6)

## [1.1.12](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/compare/1.1.11...1.1.12) - 2024-05-20

### Commits

- Release 1.1.12 [`44b4cd5`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/44b4cd55a9b8f1c1bc3436d26d2bd0b387ab7d9e)
- Updated giscom prod certificate [`e24bb13`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/e24bb13df456b1939cdff0f4fd4c7eb3ba7c63ad)

## [1.1.11](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/compare/1.1.10...1.1.11) - 2024-04-19

### Commits

- Release 1.1.11 [`02e2ca6`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/02e2ca6ac419f3936e4ed87d2d6f4fbb63cc199c)
- Fixed certificate issue Rtaa [`15f6559`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/15f65590a89da685cb7ec93f15572641dd1a18b7)

## [1.1.10](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/compare/1.1.9...1.1.10) - 2024-04-12

### Commits

- Release 1.1.10 [`5d55d29`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/5d55d291886bd2b6c5d8cdd3f9059c92982e316c)
- Update test certificate [`3512418`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/351241891c6be0f8db2ce863b39074f1be3ab924)

## [1.1.9](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/compare/1.1.8...1.1.9) - 2024-03-26

### Commits

- Release 1.1.9 [`6a14ac7`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/6a14ac79353dd319a50dfb5510381a6e10cf99ea)
- Update vallelaghi test certs [`ea8873a`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/ea8873a8bbfb3e33b772554d1a4a4aeac37b7d11)

## [1.1.8](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/compare/1.1.7...1.1.8) - 2024-01-15

### Commits

- Release 1.1.8 [`5c35023`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/5c35023947217e46dd19525f1f3ef5b53662defb)
- Updated public code version [`71a6c26`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/71a6c2688eac656cfaec1d7bf46a8a813bbf46f8)

## [1.1.7](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/compare/1.1.6...1.1.7) - 2024-01-15

### Merged

- Multiplatform builds [`#4`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/merge_requests/4)

### Commits

- Release 1.1.7 [`9b10b26`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/9b10b2619d338295a0f87c82f7ea1f6175ab83f1)
- Added multi-platform builds [`c1fe10b`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/c1fe10b49c6da1c1213754321a706345d2335874)
- Update README.md [`7a98996`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/7a9899626bcaa575785c23a46ab5b5e5bd10757f)

## [1.1.6](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/compare/1.1.5...1.1.6) - 2023-05-22

### Commits

- Added ledro certificate [`6d8b6f8`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/6d8b6f8a58777e5cc8592700d44a1e2c4644720f)

## [1.1.5](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/compare/1.1.4...1.1.5) - 2022-09-23

### Merged

- Inserito link a scheda OpenCity Italia-Stanza del cittadino su Developers Italia [`#1`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/merge_requests/1)
- Added apss [`#3`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/merge_requests/3)

### Commits

- Added rataa prod certs [`673a7f3`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/673a7f3cd7b4f587d16ea7960d6d27898dd15159)
- Apss prod [`8a900d6`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/8a900d6c1aa1cd913976552f5461baf712396f4b)

## [1.1.4](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/compare/1.1.3...1.1.4) - 2022-06-15

### Commits

- Release 1.1.4 [`4099a01`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/4099a0180d0febf5d8452095199c4b8743f60a36)
- Added apss [`cb49633`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/cb496334442a6291ea0ff180e5e286de370fca30)

## [1.1.3](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/compare/1.1.2...1.1.3) - 2022-04-05

### Commits

- Added logs [`862fc0b`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/862fc0b842bcfdef40acc81869655561975ec506)

## [1.1.2](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/compare/1.1.1...1.1.2) - 2022-03-18

### Merged

- Added test rataa [`#2`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/merge_requests/2)

## [1.1.1](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/compare/1.1.0...1.1.1) - 2021-10-28

### Commits

- Simplified CI and added publiccode test [`3a071d3`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/3a071d3b922b77f11e270f812ab832317d7f16fa)
- Aggiornata lista degli enti [`5d700c4`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/5d700c4dbbc941626d85f25a7ce75bcefa313671)
- Release 1.1.1 [`ea74c6c`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/ea74c6c9696f0c0c11aba4899e3c67863125a5a5)
- Fixed error on tests [`320491c`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/320491c68a168474aee8668ace83cf3a02d1db0c)
- Added release-it.json anche changelog file [`be24204`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/be24204d9020374e3e0025a352c3e2eae3a8f91b)
- Removed bumper [`60497df`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/60497df6a68e7b964b9c20eb2406769898a38560)
- Added API Link [`2368f3b`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/2368f3b8895ccb0bbcc434ff296c2c81131b0164)
- Avoid execute transmission in document type is different from A [`fe2d8df`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/fe2d8df3ed7de5d5f1893d379765f9bb9bbfa506)
- Added softwareVersion on publicode.yml [`d2eae6b`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/d2eae6b1ad2fc9ed4d4972beae1f7e3116b08487)
- Fix typo [`fa27e24`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/fa27e24fc415af4dd4e9e081675868c555e03c88)
- Added image [`fe62c89`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/fe62c89185ad2c6024e6e6ad38cc80e14fbdd0f8)

## 1.1.0 - 2021-10-05

### Commits

- Initial commit [`94a43ad`](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/commit/94a43adcf58f6e5cd5f59f2ff1164325c6635b0d)
