
---------------------------------------
Per compilare (jdk 1.8+)
---------------------------------------

Lanciare il task "build [default]" che crea il jar sotto build/P3WSJavaClient.jar

---------------------------------------
Per avviare
---------------------------------------

java -jar P3WSJavaClient.jar


---------------------------------------
Per testare
---------------------------------------

http://localhost:8080?method=getDocument&documentId=1290126

dovrebbe tornare un json di questo tipo:

{"data":{"id_doc":"1292520","id_file":"1293202"},"status":"success","message":"Elaborazione eseguita correttamente"}

---------------------------------------
Esempi di chiamata per tutti i metodi
---------------------------------------


----------------
createDocumentAndAddInProject

parametri accettati

	    String instance = "treville-test"; //carica i paramentri per la corretta istanza di PITre
	    String filePath = "C:\\Projects\\P.I.Tre Wrapper\\jersey-service\\src\\it\\opencontent\\p3ws\\test\\file\\test.pdf";
	    String codeNodeClassification = "1";
		String projectDescription = "Descrizione fascicolo di test 1";
		String documentDescription = "Descrizione docuento di test 1";
		String documentObj = "Oggeto del docuento di test 1";
		String[] recipientIDArray = new String[]{"554"};
		String[] trasmissionIDArray = new String[]{"554"};
		String[] recipientTypeIDArray = new String[]{"R"};
		String createProject = "false";
		String idProject = "33";


http://localhost:8080?method=createDocumentAndAddInProject&codeAdm=CCT_CAL&codeNodeClassification=1 ecc...

Esempio json ritorno
{"data":{"n_prot":"CCT_CAL|REG_CCT|23\/03\/2017|0000037|A","id_doc":"1295732","id_file":"1295732","id_proj":"1295730"},"status":"success","message":"Elaborazione eseguita correttamente"}

createDocumentAndAddInProject, qualora venga aggiunto il file, ritorna anche id_file, ovvero l'id del file aggiunto al document. In questo primo caricamento id_doc corrisponde a id_file
invece, nel caso di uploadFileToDocument id_file sar� diverso da id_doc.

createProject = true e idProject valorizzato possono coesistere, il sistema restituisce errore. createProject a true crea il primo progetto (fascicolo), idProject valorizzato fa in modo che il documento venga inserito in quel preciso fascicolo.

----------------
getDocument

	    	String codeAdm = "CCT_CAL";
	    	String documentId = "1290126";
	    	boolean getFile = false;

http://localhost:8080?method=getDocument&codeAdm=CCT_CAL&documentId=1290126&getFile=false


Esempio json ritorno

{

    "data": {
        "n_prot": "CCT_CAL|REG_CCT|14/12/2016|0000028|A",
        "id_doc": "1292520",
        "main_doc_id": "1292520",
        "main_doc_description": "Descrizione documento principale",
        "main_doc_version": "5",
        "main_doc_mime_type": "application/pdf",
        "main_doc_name": "test.pdf",
        "main_doc_content": 
		attachments": [
		
		    {
		        "attach_doc_id": "1293202",
		        "attach_doc_description": "Descrizione di test dell'allegato 1",
		        "attach_doc_version": "1",
		        "attach_doc_mime_type": "application/pdf",
		        "attach_doc_name": "test.pdf",
		        "attach_doc_content": 
            },
		    {
		        "attach_doc_id": "1293203",
		        "attach_doc_description": "Descrizione di test dell'allegato 2",
		        "attach_doc_version": "2",
		        "attach_doc_mime_type": "application/pdf",
		        "attach_doc_name": "test.pdf",
		        "attach_doc_content": 
            }
        ]
    },
    "status": "success",
    "message": "Elaborazione eseguita correttamente"

}


----------------
uploadFileToDocument

	    	String instance = "treville-test";
	    	String documentId="1290126";
	    	String filePath="C:\\Projects\\P.I.Tre Wrapper\\jersey-service\\src\\it\\opencontent\\p3ws\\test\\file\\test.pdf";
	    	String attachmentDescription = "Descrizione di test dell'allegato";
	    	boolean createAttachment = true;
	    	String attachmentType = "U";

http://localhost:8080?method=uploadFileToDocument&instance=treville-test&documentId=1290126&attachmentDescription=Descrizione di test dell'allegato& ecc..

Esempio json ritorno

{"data":{"id_doc":"1292520","id_file":"1293202"},"status":"success","message":"Elaborazione eseguita correttamente"}

id_file � l'id dell'allegato appena creato, il metodo da per scontato che l'ultimo file della lista degli attachment sia quello appena creato


---------------------------------------
Utilities
---------------------------------------

Come creare il trustore dato il certificato .p12

"C:\Program Files\Java\jdk1.8.0_102\bin\keytool" -import -alias truststore-t-tvallelaghi -file Informatica_Trentina_CA2.jks -keystore truststore-t-tvallelaghi -storepass 4O7k3cC8UE

Il "Informatica_Trentina_CA2.jks" va preso collegandosi all'indirizzo web di PITre e non dal .p12 che viene fornito.

attenzione perch� in test da Eclipse occorre mettere il jar generato nel filepath per fargli trovare (runtime) il certificato trustsotre generato

---------------------------------------
Generazione dei jar
---------------------------------------

Il file build.xml (task build [default]) genera il jar build/P3WSJavaClient.jar contenente gi� i paramentri di connessione e i certificati per i vari ambienti.
I dati dei vari ambienti vanno impostati in P3WSClient.java prima della generazione del JAR.
Il JAR poi va rinominato manualmente per distinguere le istanze (test e prod)

---------------------------------------
Problemi noti
---------------------------------------

Sembra che se non si lancia il jar con la stessa versione della jdk con cui è stato compilato dia errore di certificato

- Il build crea un jar eseguibile build/P3WSJavaClient.jar privo di librerie da cui dipende
- Le librerie vengono messe in build/lib
- Il MANIFEST.MF punta a queste lib (in Windows riesco a mettere le lib internamente a P3WSJavaClient.jar e farle puntare, in unix non riesco a dargli il path corretto per cui opto per mettere le lib esternamente)

