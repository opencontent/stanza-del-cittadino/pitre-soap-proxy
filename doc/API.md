# Definizione API

### createDocumentAndAddInProject

| Parametro | Tipo | Descrizione | Esempio |
| ------ | ------ | ------ | ------ |
| ```filePath``` | stringa | Percorso del file locale da caricare come documento principale | "/home/developer/test.pdf" |
| ```codeNodeClassification``` | stringa | Ramo del titolario nel quale vengono creati progetti e documenti | "1" |
| ```projectDescription``` | stringa | Descrizione del fascicolo | "Descrizione fascicolo di test 1" |
| ```documentDescription``` | stringa | Descrizione del documento principale | "Descrizione documento di test 1" |
| ```documentObj``` | stringa | Oggetto del documento principale | "Oggetto del documento di test 1" |
| ```recipientIDArray``` | array | Array di codici dei ruoli/uffici/persone a cui è destinato il documento | ["554"] |
| ```trasmissionIDArray``` | array | Array di codici dei ruoli/uffici/persone a cui viene trasmesso il documento | ["554"] |
| ```recipientTypeIDArray``` | array | Tipologia di destinatario del documento R=ruolo, U=Ufficio ecc... | ["R"] |
| ```createProject``` | booleano | Crea il fascicolo | false |
| ```idProject``` | stringa | Aggiorna il fascicolo | "33" |
| ```instance``` | stringa | Nome della configurazione | "treville-test" |
| ~~```codeAdm```~~ | stringa | Codice amministrazione (viene ignorato da quando è stata inserito il parametro instance) | "CCT_CAL" |

### uploadFileToDocument

| Parametro | Tipo | Descrizione | Esempio |
| ------ | ------ | ------ | ------ |
| ```documentId``` | stringa | Id del documento | "1290126" |
| ```instance``` | stringa | Nome della configurazione | "treville-test" |
| ```filePath``` | stringa | Percorso del file locale da caricare | "/home/developer/test.pdf" |
| ```attachmentDescription``` | stringa | Descrizione dell'allegato | "Descrizione di test dell'allegato" |
| ```createAttachment``` | booleano | Crea allegato | true |
| ```attachmentType``` | string | Tipologia di allegato U = utente, E = esterno | "U" |
| ~~```codeAdm```~~ | stringa | Codice amministrazione (viene ignorato da quando è stata inserito il parametro instance) | "CCT_CAL" |


### getDocument

| Parametro | Tipo | Descrizione | Esempio |
| ------ | ------ | ------ | ------ |
| ```documentId``` | stringa | Id del documento | "1290126" |
| ```instance``` | stringa | Nome della configurazione | "treville-test" |
| ```getFile``` | booleano | Restituisce il contenuto del file | false |
| ~~```codeAdm```~~ | stringa | Codice amministrazione (viene ignorato da quando è stata inserito il parametro instance) | "CCT_CAL" |
