Quando si richiama un servizio con protocollo https ci possono essere i seguenti casi : 
a) 
 Nel caso i servizi richiedano la chiave privata per autenticazione/integrità dei dati 
 bisogna eseguire i seguenti passi  (in questo caso si parla di : Secret Key Cryptography -> symmetric cryptography ).: 

 1) importare il keystore PKCS12 che contiene la coppia chiave pubblica/privata nel keystore di java con il seguente comando 
  keytool -v -importkeystore -srckeystore <<keyPK12>> -srcstoretype PKCS12 -destkeystore <<keystorejavadest>> -deststoretype JKS -srcstorepass <<pwdkeystorepkcs12>> -deststorepass <<pwdkeystorejava>> -srcalias <<aliascoppiakeyorgine>> -destalias <<aliascoppiakeydest>> -destkeypass <<pwdkeystorejava>>
  

 <<keyPK12>> : il percorso dove si trova il nome del keystore PKCS12/nome del file es ./certicato.p12 
 <<keystorejavadest>> : il percorso del keystore java dove si vuole fare l'importo 
 <<pwdkeystorepkcs12>>: la password del keystore PKCS12
 <<pwdkeystorejava>>: password del keystore java di destinazione
 <<aliascoppiakeyorgine>>: l'alias utilizzato nel keystore PKCS12 per la coppia di chiavi che si vuole importare
 <<aliascoppiakeydest>>: il nuovo alias nel keystore java delle chiavi da importare 
 N.B. IMPORTANTE destkeypass == deststorepass == <<keystorejavadest>>

 2) Importare il certificato PUBBLICO (Di Informatica Trentina o di altre CA) nel keystore (certificato per i meccanismi dei server trust)
 
  -import -alias <<alias>> -file <<cert_file>> -keystore <<keystore>>  -storepass <<storepass>> 
  
  <<alias>> : alias del certificato da importare , non può esistere un alias con lo stesso nome nel keystore
  <<cert_file>> : il file contenente il certificato (formato x.509)
  <<storepass>> : la password del keystore 
  <<keystore>> : il riferimento al keystore 


 Il keystore del passo 1) serve per cifrare le chiamate inviate dal client a server e si chiama <<keyStore>>
 mentre il keystore del passo 2) server per riconoscere il server remoto, ovvero se il client riconosce il server come 
 un'entità valida, si chiama <<trustStore>>

 Adesso bisogna opportunamente configurare la jvm impostando le variabili di sistema

 -Djavax.net.ssl.keyStoreType=jks
 -Djavax.net.ssl.trustStoreType=jks
 -Djavax.net.ssl.keyStore=<<keyStore>>
 -Djavax.net.ssl.trustStore=<<trustStore>>
 -Djavax.net.debug=ssl 
 -Djavax.net.ssl.keyStorePassword=<<pwdkeyStore>>
 -Djavax.net.ssl.trustStorePassword=<<pwdtrustStore>>

 In linea del tutto generale di può di avere <<keyStore>>  e <<trustStore>> nello stesso key store 
 di conseguenza si ha 
 -Djavax.net.ssl.keyStoreType=jks
 -Djavax.net.ssl.trustStoreType=jks
 -Djavax.net.ssl.keyStore=<<keyStore>>
 -Djavax.net.ssl.trustStore=<<keyStore>>
 -Djavax.net.debug=ssl 
 -Djavax.net.ssl.keyStorePassword=<<pwdkeyStore>>
 -Djavax.net.ssl.trustStorePassword=<<pwdkeyStore>>

 Ovviamente nessuno vieta di non eseguire il passo 1) e i valori delle variabili diventano 

 -Djavax.net.ssl.keyStoreType=pkcs12
 -Djavax.net.ssl.trustStoreType=jks
 -Djavax.net.ssl.keyStore=<<keyStore>>
 -Djavax.net.ssl.trustStore=<<trustStore>>
 -Djavax.net.debug=ssl 
 -Djavax.net.ssl.keyStorePassword=<<pwdkeyStore>>
 -Djavax.net.ssl.trustStorePassword=<<pwdtrustStore>>

 <<keyStore>> : in questo caso è il valore del keystore PKCS12 es ./certicato.p12 

 Tutto questo si semplifica ovviamente se si utilizza il keystore di default della java 
 in questo caso nn bisogna impostare nessuna variabile di sistema 
 es. import del certificato nel keystore di default del passo 2) 
  keytool -import -trustcacerts -file <<cert_file>> -alias <<alias>> -keystore $JAVA_HOME/jre/lib/security/cacerts -storepass <<storepass>> 
 

b) Nel caso i servizi NON  richiedano la chiave privata ma solo "decisioni" sui server che devono essere trust 
 allora non bisogna eseguire il passo 1(in questo caso si parla di : Public Key Cryptography -> asymmetric cryptography ).


P.S.
Tutti i paramentri da impostare come parametri della JVM possono anche essere impostati da codice utilizzando la seguente sintassi 
System.setProperty(<<propertyName>>, <<propertyValue>>);
es. <<propertyName>> = javax.net.ssl.keyStore
 <<propertyValue>> = ./certicato.p12
