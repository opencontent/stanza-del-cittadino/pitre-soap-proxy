# PITreWrapper - DEPRECATO

[![pipeline status](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/badges/main/pipeline.svg)](https://gitlab.com/opencontent/stanza-del-cittadino/pitre-soap-proxy/-/commits/main)

Proxy per l'interazione con il software di protocollo Pitre

## Descrizione

Dialoga con protocollo SOAP verso il servizio Pitre, sul quale si autentica con client-certificate
e credenziali fornite dal gestore. Verso Stanza del Cittadino dialoga invece con protocollo ReST.

## Installazione

Si tratta di un servizio _stateless_ quindi non richiede database di alcun tipo.

Viene rilasciato come immagine Docker, per una installazione senza docker è sufficiente analizzare
il dockerfile per dedurre i requisiti di sistema necessari.

## Configurazione

Nella directory dove gira l'applicativo devono essere presenti le directory `properties` e `certificate` contenenti rispettivamente i file di configurazione dei tenant e i certificati a cui si fa riferimenti nei file properties.

## Stato del progetto

Il progetto è usato in **produzione** dal 2017 ma non è più **mantenuto**. 

Non sono previste evolutive, la nuova interfaccia verso il protocollo Pitre implementa il protocollo 
ReST ed è attualmente in produzione, vedi per questo [Application Registry](https://gitlab.com/opencity-labs/area-personale/stanzadelcittadino-application-registry).

## Copyright

Copyright (C) 2016-2020  Opencontent SCARL

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


