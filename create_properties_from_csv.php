<?php

$csvFile = __DIR__ . '/properties.csv';
$templatesDir = __DIR__ . '/properties_templates';
$propertiesDir = __DIR__ . '/properties';

$headers = [
  'instance',
  'env',
  'wsURL',
  'keyStore',
  'trustStore',
  'keyStorePassword',
  'trustStorePassword',
  'codeAdm',
  'codeRoleLogin',
  'codeRegister',
  'userName',
  'userID',
];

function generateFileName($data)
{
  global $propertiesDir;

  return $propertiesDir . '/' . $data[0] . '-' . $data[1] . '-config.properties';
}

function getTemplateData($data)
{
  global $templatesDir;
  static $templateData;

  $env = $data[1];

  if (isset($templateData[$env])) {
    return $templateData[$env];
  }
  $template = [];
  if (file_exists($templatesDir . '/' . $env)) {
    $contents = file_get_contents($templatesDir . '/' . $env);
    $rows = explode("\n", $contents);
    foreach ($rows as $row) {
      if (empty($row)) {
        continue;
      }
      list($key, $value) = explode('=', $row, 2);
      $template[$key] = trim($value);
    }
  }

  $templateData[$env] = $template;

  return $templateData[$env];
}

$headersCount = count($headers);
$row = 0;
if (($handle = fopen($csvFile, "r")) !== false) {
  while (($data = fgetcsv($handle, 1000, ",")) !== false) {
    $row++;
    if ($row === 1){
      continue;
    }
    $template = getTemplateData($data);
    $file = generateFileName($data);
    $contentsArray = [];
    for ($i = 0; $i < $headersCount; $i++) {
      if ($i > 1) {
        if (empty($data[$i]) && isset($template[$headers[$i]])){
          $data[$i] = $template[$headers[$i]];
        }
        $contentsArray[] = $headers[$i] . '=' . $data[$i];
      }
    }

    file_put_contents($file, implode("\n", $contentsArray) . "\n");
  }
  fclose($handle);
}
