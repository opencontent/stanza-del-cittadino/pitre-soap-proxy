<?php

$propertiesDir = __DIR__ . '/properties';

$headers = [
  'instance',
  'env',
  'wsURL',
  'keyStore',
  'trustStore',
  'keyStorePassword',
  'trustStorePassword',
  'codeAdm',
  'codeRoleLogin',
  'codeRegister',
  'userName',
  'userID',
];


$propertiesFiles = [];
if ($handle = @opendir($propertiesDir)) {
  while (($element = readdir($handle)) !== false) {
    if (
      $element === '.'
      || $element === '..'
      || $element[0] === '.'
      || is_dir($propertiesDir . '/' . $element)
      || is_link($propertiesDir . '/' . $element)
    ) {
      continue;
    }
    $propertiesFiles[] = $propertiesDir . '/' . $element;
  }
  @closedir($handle);
}

$data = [];

foreach ($propertiesFiles as $file) {
  $instance = [];
  list($instanceIdentifier, $env) = explode('-', basename($file));
  $instance['instance'] = $instanceIdentifier;
  $instance['env'] = $env;
  $contents = file_get_contents($file);
  $rows = explode("\n", $contents);
  foreach ($rows as $row) {
    if (empty($row)) {
      continue;
    }
    list($key, $value) = explode('=', $row, 2);
    if (in_array($key, $headers)) {
      $instance[$key] = trim($value);
    }
  }
  $data[$instanceIdentifier . '-' . $env] = $instance;
}

ksort($data);
$fp = fopen('properties.csv', 'w');
fputcsv($fp, $headers);
foreach ($data as $instance) {
  fputcsv($fp, array_values($instance));
}
fclose($fp);
