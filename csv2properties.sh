#!/usr/bin/env bash

[[ $DEBUG ]] && set -ex

csv_file=${SOURCE_FILE:-properties.csv}
[[ ! -d ./properties ]] && mkdir ./properties

cat $csv_file | while read line
do
  instance=$(echo $line | cut -d',' -f1 | tr -d '"')  
  env=$(echo $line | cut -d',' -f2 | tr -d '"')  
  wsURL=$(echo $line | cut -d',' -f3 | tr -d '"')  
  keyStore=$(echo $line | cut -d',' -f4 | tr -d '"')  
  trustStore=$(echo $line | cut -d',' -f5 | tr -d '"')
  keyStorePassword=$(echo $line | cut -d',' -f6 | tr -d '"')  
  trustStorePassword=$(echo $line | cut -d',' -f7 | tr -d '"')  
  codeAdm=$(echo $line | cut -d',' -f8 | tr -d '"')  
  codeRoleLogin=$(echo $line | cut -d',' -f9 | tr -d '"')  
  codeRegister=$(echo $line | cut -d',' -f10 | tr -d '"')  
  userName=$(echo $line | cut -d',' -f11 | tr -d '"')  
  userID=$(echo $line | cut -d',' -f12 | tr -d '"')

  properties_file="./properties/${instance}-${env}-config.properties"
  if [[ ! -f $properties_file ]]; then
    if ! touch $properties_file; then
      echo "ERROR, cannot write file $properties_file"
      exit 1
    fi
    
    for item in wsURL keyStore trustStore keyStorePassword trustStorePassword codeAdm codeRoleLogin codeRegister userName userID; do
      eval "value=\$$item"
      [[ -z $value ]] && echo "Warning, null value for $item, double check plz!"
      echo "$item=$value" >> $properties_file 
    done
    echo "Instance $instance (env $env) -> wrote file: $properties_file"
  fi
done
