
/**
 * AddressBookCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */

    package it.opencontent.nttdata._2012.pi3;

    /**
     *  AddressBookCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class AddressBookCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public AddressBookCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public AddressBookCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getCorrespondent method
            * override this method for handling normal response from getCorrespondent operation
            */
           public void receiveResultgetCorrespondent(
                    it.opencontent.nttdata._2012.pi3.GetCorrespondentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCorrespondent operation
           */
            public void receiveErrorgetCorrespondent(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addCorrespondent method
            * override this method for handling normal response from addCorrespondent operation
            */
           public void receiveResultaddCorrespondent(
                    it.opencontent.nttdata._2012.pi3.AddCorrespondentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addCorrespondent operation
           */
            public void receiveErroraddCorrespondent(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for searchUsers method
            * override this method for handling normal response from searchUsers operation
            */
           public void receiveResultsearchUsers(
                    it.opencontent.nttdata._2012.pi3.SearchUsersResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from searchUsers operation
           */
            public void receiveErrorsearchUsers(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCorrespondentFilters method
            * override this method for handling normal response from getCorrespondentFilters operation
            */
           public void receiveResultgetCorrespondentFilters(
                    it.opencontent.nttdata._2012.pi3.GetCorrespondentFiltersResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCorrespondentFilters operation
           */
            public void receiveErrorgetCorrespondentFilters(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for deleteCorrespondent method
            * override this method for handling normal response from deleteCorrespondent operation
            */
           public void receiveResultdeleteCorrespondent(
                    it.opencontent.nttdata._2012.pi3.DeleteCorrespondentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from deleteCorrespondent operation
           */
            public void receiveErrordeleteCorrespondent(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for editCorrespondent method
            * override this method for handling normal response from editCorrespondent operation
            */
           public void receiveResulteditCorrespondent(
                    it.opencontent.nttdata._2012.pi3.EditCorrespondentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from editCorrespondent operation
           */
            public void receiveErroreditCorrespondent(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getUserFilters method
            * override this method for handling normal response from getUserFilters operation
            */
           public void receiveResultgetUserFilters(
                    it.opencontent.nttdata._2012.pi3.GetUserFiltersResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUserFilters operation
           */
            public void receiveErrorgetUserFilters(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getOpportunityList method
            * override this method for handling normal response from getOpportunityList operation
            */
           public void receiveResultgetOpportunityList(
                    it.opencontent.nttdata._2012.pi3.GetOpportunityListResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOpportunityList operation
           */
            public void receiveErrorgetOpportunityList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for searchCorrespondents method
            * override this method for handling normal response from searchCorrespondents operation
            */
           public void receiveResultsearchCorrespondents(
                    it.opencontent.nttdata._2012.pi3.SearchCorrespondentsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from searchCorrespondents operation
           */
            public void receiveErrorsearchCorrespondents(java.lang.Exception e) {
            }
                


    }
    