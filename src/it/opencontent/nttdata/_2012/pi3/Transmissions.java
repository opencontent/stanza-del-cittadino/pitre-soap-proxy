

/**
 * Transmissions.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */

    package it.opencontent.nttdata._2012.pi3;

    /*
     *  Transmissions java interface
     */

    public interface Transmissions {
          

        /**
          * Auto generated method signature
          * 
                    * @param executeTransmissionProject0
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.ExecuteTransmissionProjectResponse executeTransmissionProject(

                        it.opencontent.nttdata._2012.pi3.ExecuteTransmissionProject executeTransmissionProject0)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param executeTransmissionProject0
            
          */
        public void startexecuteTransmissionProject(

            it.opencontent.nttdata._2012.pi3.ExecuteTransmissionProject executeTransmissionProject0,

            final it.opencontent.nttdata._2012.pi3.TransmissionsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param giveUpRights2
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GiveUpRightsResponse giveUpRights(

                        it.opencontent.nttdata._2012.pi3.GiveUpRights giveUpRights2)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param giveUpRights2
            
          */
        public void startgiveUpRights(

            it.opencontent.nttdata._2012.pi3.GiveUpRights giveUpRights2,

            final it.opencontent.nttdata._2012.pi3.TransmissionsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param executeTransmPrjModel4
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.ExecuteTransmPrjModelResponse executeTransmPrjModel(

                        it.opencontent.nttdata._2012.pi3.ExecuteTransmPrjModel executeTransmPrjModel4)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param executeTransmPrjModel4
            
          */
        public void startexecuteTransmPrjModel(

            it.opencontent.nttdata._2012.pi3.ExecuteTransmPrjModel executeTransmPrjModel4,

            final it.opencontent.nttdata._2012.pi3.TransmissionsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getTransmissionModel6
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetTransmissionModelResponse getTransmissionModel(

                        it.opencontent.nttdata._2012.pi3.GetTransmissionModel getTransmissionModel6)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getTransmissionModel6
            
          */
        public void startgetTransmissionModel(

            it.opencontent.nttdata._2012.pi3.GetTransmissionModel getTransmissionModel6,

            final it.opencontent.nttdata._2012.pi3.TransmissionsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getTransmissionModels8
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetTransmissionModelsResponse getTransmissionModels(

                        it.opencontent.nttdata._2012.pi3.GetTransmissionModels getTransmissionModels8)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getTransmissionModels8
            
          */
        public void startgetTransmissionModels(

            it.opencontent.nttdata._2012.pi3.GetTransmissionModels getTransmissionModels8,

            final it.opencontent.nttdata._2012.pi3.TransmissionsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param executeTransmissionDocument10
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.ExecuteTransmissionDocumentResponse executeTransmissionDocument(

                        it.opencontent.nttdata._2012.pi3.ExecuteTransmissionDocument executeTransmissionDocument10)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param executeTransmissionDocument10
            
          */
        public void startexecuteTransmissionDocument(

            it.opencontent.nttdata._2012.pi3.ExecuteTransmissionDocument executeTransmissionDocument10,

            final it.opencontent.nttdata._2012.pi3.TransmissionsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param executeTransmDocModel12
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.ExecuteTransmDocModelResponse executeTransmDocModel(

                        it.opencontent.nttdata._2012.pi3.ExecuteTransmDocModel executeTransmDocModel12)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param executeTransmDocModel12
            
          */
        public void startexecuteTransmDocModel(

            it.opencontent.nttdata._2012.pi3.ExecuteTransmDocModel executeTransmDocModel12,

            final it.opencontent.nttdata._2012.pi3.TransmissionsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        
       //
       }
    