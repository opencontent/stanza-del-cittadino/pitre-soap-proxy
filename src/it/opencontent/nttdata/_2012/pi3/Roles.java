

/**
 * Roles.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */

    package it.opencontent.nttdata._2012.pi3;

    /*
     *  Roles java interface
     */

    public interface Roles {
          

        /**
          * Auto generated method signature
          * 
                    * @param getUsersInRole0
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetUsersInRoleResponse getUsersInRole(

                        it.opencontent.nttdata._2012.pi3.GetUsersInRole getUsersInRole0)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getUsersInRole0
            
          */
        public void startgetUsersInRole(

            it.opencontent.nttdata._2012.pi3.GetUsersInRole getUsersInRole0,

            final it.opencontent.nttdata._2012.pi3.RolesCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getRoles2
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetRolesResponse getRoles(

                        it.opencontent.nttdata._2012.pi3.GetRoles getRoles2)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getRoles2
            
          */
        public void startgetRoles(

            it.opencontent.nttdata._2012.pi3.GetRoles getRoles2,

            final it.opencontent.nttdata._2012.pi3.RolesCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getRolesForEnabledActions4
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetRolesForEnabledActionsResponse getRolesForEnabledActions(

                        it.opencontent.nttdata._2012.pi3.GetRolesForEnabledActions getRolesForEnabledActions4)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getRolesForEnabledActions4
            
          */
        public void startgetRolesForEnabledActions(

            it.opencontent.nttdata._2012.pi3.GetRolesForEnabledActions getRolesForEnabledActions4,

            final it.opencontent.nttdata._2012.pi3.RolesCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getRole6
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetRoleResponse getRole(

                        it.opencontent.nttdata._2012.pi3.GetRole getRole6)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getRole6
            
          */
        public void startgetRole(

            it.opencontent.nttdata._2012.pi3.GetRole getRole6,

            final it.opencontent.nttdata._2012.pi3.RolesCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        
       //
       }
    