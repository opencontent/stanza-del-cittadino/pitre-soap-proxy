

/**
 * Projects.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */

    package it.opencontent.nttdata._2012.pi3;

    /*
     *  Projects java interface
     */

    public interface Projects {
          

        /**
          * Auto generated method signature
          * 
                    * @param followProject0
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.FollowProjectResponse followProject(

                        it.opencontent.nttdata._2012.pi3.FollowProject followProject0)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param followProject0
            
          */
        public void startfollowProject(

            it.opencontent.nttdata._2012.pi3.FollowProject followProject0,

            final it.opencontent.nttdata._2012.pi3.ProjectsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getProjectStateDiagram2
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetProjectStateDiagramResponse getProjectStateDiagram(

                        it.opencontent.nttdata._2012.pi3.GetProjectStateDiagram getProjectStateDiagram2)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getProjectStateDiagram2
            
          */
        public void startgetProjectStateDiagram(

            it.opencontent.nttdata._2012.pi3.GetProjectStateDiagram getProjectStateDiagram2,

            final it.opencontent.nttdata._2012.pi3.ProjectsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param editProject4
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.EditProjectResponse editProject(

                        it.opencontent.nttdata._2012.pi3.EditProject editProject4)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param editProject4
            
          */
        public void starteditProject(

            it.opencontent.nttdata._2012.pi3.EditProject editProject4,

            final it.opencontent.nttdata._2012.pi3.ProjectsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getProjectsByDocument6
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetProjectsByDocumentResponse getProjectsByDocument(

                        it.opencontent.nttdata._2012.pi3.GetProjectsByDocument getProjectsByDocument6)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getProjectsByDocument6
            
          */
        public void startgetProjectsByDocument(

            it.opencontent.nttdata._2012.pi3.GetProjectsByDocument getProjectsByDocument6,

            final it.opencontent.nttdata._2012.pi3.ProjectsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param openCloseProject8
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.OpenCloseProjectResponse openCloseProject(

                        it.opencontent.nttdata._2012.pi3.OpenCloseProject openCloseProject8)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param openCloseProject8
            
          */
        public void startopenCloseProject(

            it.opencontent.nttdata._2012.pi3.OpenCloseProject openCloseProject8,

            final it.opencontent.nttdata._2012.pi3.ProjectsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getTemplatePrj10
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetTemplatePrjResponse getTemplatePrj(

                        it.opencontent.nttdata._2012.pi3.GetTemplatePrj getTemplatePrj10)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getTemplatePrj10
            
          */
        public void startgetTemplatePrj(

            it.opencontent.nttdata._2012.pi3.GetTemplatePrj getTemplatePrj10,

            final it.opencontent.nttdata._2012.pi3.ProjectsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getProjectFilters12
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetProjectFiltersResponse getProjectFilters(

                        it.opencontent.nttdata._2012.pi3.GetProjectFilters getProjectFilters12)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getProjectFilters12
            
          */
        public void startgetProjectFilters(

            it.opencontent.nttdata._2012.pi3.GetProjectFilters getProjectFilters12,

            final it.opencontent.nttdata._2012.pi3.ProjectsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getProject14
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetProjectResponse getProject(

                        it.opencontent.nttdata._2012.pi3.GetProject getProject14)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getProject14
            
          */
        public void startgetProject(

            it.opencontent.nttdata._2012.pi3.GetProject getProject14,

            final it.opencontent.nttdata._2012.pi3.ProjectsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param searchProjects16
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.SearchProjectsResponse searchProjects(

                        it.opencontent.nttdata._2012.pi3.SearchProjects searchProjects16)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param searchProjects16
            
          */
        public void startsearchProjects(

            it.opencontent.nttdata._2012.pi3.SearchProjects searchProjects16,

            final it.opencontent.nttdata._2012.pi3.ProjectsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param createProject18
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.CreateProjectResponse createProject(

                        it.opencontent.nttdata._2012.pi3.CreateProject createProject18)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param createProject18
            
          */
        public void startcreateProject(

            it.opencontent.nttdata._2012.pi3.CreateProject createProject18,

            final it.opencontent.nttdata._2012.pi3.ProjectsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getTemplatesProjects20
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetTemplatesProjectsResponse getTemplatesProjects(

                        it.opencontent.nttdata._2012.pi3.GetTemplatesProjects getTemplatesProjects20)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getTemplatesProjects20
            
          */
        public void startgetTemplatesProjects(

            it.opencontent.nttdata._2012.pi3.GetTemplatesProjects getTemplatesProjects20,

            final it.opencontent.nttdata._2012.pi3.ProjectsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getLinkPrjByID22
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetLinkPrjByIDResponse getLinkPrjByID(

                        it.opencontent.nttdata._2012.pi3.GetLinkPrjByID getLinkPrjByID22)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getLinkPrjByID22
            
          */
        public void startgetLinkPrjByID(

            it.opencontent.nttdata._2012.pi3.GetLinkPrjByID getLinkPrjByID22,

            final it.opencontent.nttdata._2012.pi3.ProjectsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param editPrjStateDiagram24
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.EditPrjStateDiagramResponse editPrjStateDiagram(

                        it.opencontent.nttdata._2012.pi3.EditPrjStateDiagram editPrjStateDiagram24)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param editPrjStateDiagram24
            
          */
        public void starteditPrjStateDiagram(

            it.opencontent.nttdata._2012.pi3.EditPrjStateDiagram editPrjStateDiagram24,

            final it.opencontent.nttdata._2012.pi3.ProjectsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        
       //
       }
    