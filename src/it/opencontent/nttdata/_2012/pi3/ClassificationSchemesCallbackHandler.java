
/**
 * ClassificationSchemesCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */

    package it.opencontent.nttdata._2012.pi3;

    /**
     *  ClassificationSchemesCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ClassificationSchemesCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ClassificationSchemesCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ClassificationSchemesCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getClassificationSchemeById method
            * override this method for handling normal response from getClassificationSchemeById operation
            */
           public void receiveResultgetClassificationSchemeById(
                    it.opencontent.nttdata._2012.pi3.GetClassificationSchemeByIdResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getClassificationSchemeById operation
           */
            public void receiveErrorgetClassificationSchemeById(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getActiveClassificationScheme method
            * override this method for handling normal response from getActiveClassificationScheme operation
            */
           public void receiveResultgetActiveClassificationScheme(
                    it.opencontent.nttdata._2012.pi3.GetActiveClassificationSchemeResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getActiveClassificationScheme operation
           */
            public void receiveErrorgetActiveClassificationScheme(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAllClassificationSchemes method
            * override this method for handling normal response from getAllClassificationSchemes operation
            */
           public void receiveResultgetAllClassificationSchemes(
                    it.opencontent.nttdata._2012.pi3.GetAllClassificationSchemesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAllClassificationSchemes operation
           */
            public void receiveErrorgetAllClassificationSchemes(java.lang.Exception e) {
            }
                


    }
    