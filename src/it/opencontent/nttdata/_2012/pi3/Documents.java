

/**
 * Documents.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */

    package it.opencontent.nttdata._2012.pi3;

    /*
     *  Documents java interface
     */

    public interface Documents {
          

        /**
          * Auto generated method signature
          * 
                    * @param getDocumentFilters0
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetDocumentFiltersResponse getDocumentFilters(

                        it.opencontent.nttdata._2012.pi3.GetDocumentFilters getDocumentFilters0)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getDocumentFilters0
            
          */
        public void startgetDocumentFilters(

            it.opencontent.nttdata._2012.pi3.GetDocumentFilters getDocumentFilters0,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getTemplateDoc2
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetTemplateDocResponse getTemplateDoc(

                        it.opencontent.nttdata._2012.pi3.GetTemplateDoc getTemplateDoc2)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getTemplateDoc2
            
          */
        public void startgetTemplateDoc(

            it.opencontent.nttdata._2012.pi3.GetTemplateDoc getTemplateDoc2,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param editDocStateDiagram4
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.EditDocStateDiagramResponse editDocStateDiagram(

                        it.opencontent.nttdata._2012.pi3.EditDocStateDiagram editDocStateDiagram4)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param editDocStateDiagram4
            
          */
        public void starteditDocStateDiagram(

            it.opencontent.nttdata._2012.pi3.EditDocStateDiagram editDocStateDiagram4,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param createDocumentAndAddInProject6
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.CreateDocumentAndAddInProjectResponse createDocumentAndAddInProject(

                        it.opencontent.nttdata._2012.pi3.CreateDocumentAndAddInProject createDocumentAndAddInProject6)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param createDocumentAndAddInProject6
            
          */
        public void startcreateDocumentAndAddInProject(

            it.opencontent.nttdata._2012.pi3.CreateDocumentAndAddInProject createDocumentAndAddInProject6,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getLinkDocByID8
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetLinkDocByIDResponse getLinkDocByID(

                        it.opencontent.nttdata._2012.pi3.GetLinkDocByID getLinkDocByID8)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getLinkDocByID8
            
          */
        public void startgetLinkDocByID(

            it.opencontent.nttdata._2012.pi3.GetLinkDocByID getLinkDocByID8,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getEnvelopedFileById10
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetEnvelopedFileByIdResponse getEnvelopedFileById(

                        it.opencontent.nttdata._2012.pi3.GetEnvelopedFileById getEnvelopedFileById10)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getEnvelopedFileById10
            
          */
        public void startgetEnvelopedFileById(

            it.opencontent.nttdata._2012.pi3.GetEnvelopedFileById getEnvelopedFileById10,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param createDocument12
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.CreateDocumentResponse createDocument(

                        it.opencontent.nttdata._2012.pi3.CreateDocument createDocument12)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param createDocument12
            
          */
        public void startcreateDocument(

            it.opencontent.nttdata._2012.pi3.CreateDocument createDocument12,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param sendDocument14
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.SendDocumentResponse sendDocument(

                        it.opencontent.nttdata._2012.pi3.SendDocument sendDocument14)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param sendDocument14
            
          */
        public void startsendDocument(

            it.opencontent.nttdata._2012.pi3.SendDocument sendDocument14,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param editDocument16
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.EditDocumentResponse editDocument(

                        it.opencontent.nttdata._2012.pi3.EditDocument editDocument16)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param editDocument16
            
          */
        public void starteditDocument(

            it.opencontent.nttdata._2012.pi3.EditDocument editDocument16,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getDocument18
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetDocumentResponse getDocument(

                        it.opencontent.nttdata._2012.pi3.GetDocument getDocument18)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getDocument18
            
          */
        public void startgetDocument(

            it.opencontent.nttdata._2012.pi3.GetDocument getDocument18,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param uploadFileToDocument20
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.UploadFileToDocumentResponse uploadFileToDocument(

                        it.opencontent.nttdata._2012.pi3.UploadFileToDocument uploadFileToDocument20)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param uploadFileToDocument20
            
          */
        public void startuploadFileToDocument(

            it.opencontent.nttdata._2012.pi3.UploadFileToDocument uploadFileToDocument20,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param followDocument22
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.FollowDocumentResponse followDocument(

                        it.opencontent.nttdata._2012.pi3.FollowDocument followDocument22)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param followDocument22
            
          */
        public void startfollowDocument(

            it.opencontent.nttdata._2012.pi3.FollowDocument followDocument22,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getDocumentStateDiagram24
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetDocumentStateDiagramResponse getDocumentStateDiagram(

                        it.opencontent.nttdata._2012.pi3.GetDocumentStateDiagram getDocumentStateDiagram24)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getDocumentStateDiagram24
            
          */
        public void startgetDocumentStateDiagram(

            it.opencontent.nttdata._2012.pi3.GetDocumentStateDiagram getDocumentStateDiagram24,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param searchDocuments26
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.SearchDocumentsResponse searchDocuments(

                        it.opencontent.nttdata._2012.pi3.SearchDocuments searchDocuments26)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param searchDocuments26
            
          */
        public void startsearchDocuments(

            it.opencontent.nttdata._2012.pi3.SearchDocuments searchDocuments26,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param uploadFileToDocumentFromWord28
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.UploadFileToDocumentFromWordResponse uploadFileToDocumentFromWord(

                        it.opencontent.nttdata._2012.pi3.UploadFileToDocumentFromWord uploadFileToDocumentFromWord28)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param uploadFileToDocumentFromWord28
            
          */
        public void startuploadFileToDocumentFromWord(

            it.opencontent.nttdata._2012.pi3.UploadFileToDocumentFromWord uploadFileToDocumentFromWord28,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getStampAndSignature30
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetStampAndSignatureResponse getStampAndSignature(

                        it.opencontent.nttdata._2012.pi3.GetStampAndSignature getStampAndSignature30)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getStampAndSignature30
            
          */
        public void startgetStampAndSignature(

            it.opencontent.nttdata._2012.pi3.GetStampAndSignature getStampAndSignature30,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param addDocInProject32
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.AddDocInProjectResponse addDocInProject(

                        it.opencontent.nttdata._2012.pi3.AddDocInProject addDocInProject32)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param addDocInProject32
            
          */
        public void startaddDocInProject(

            it.opencontent.nttdata._2012.pi3.AddDocInProject addDocInProject32,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getTemplatesDocuments34
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetTemplatesDocumentsResponse getTemplatesDocuments(

                        it.opencontent.nttdata._2012.pi3.GetTemplatesDocuments getTemplatesDocuments34)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getTemplatesDocuments34
            
          */
        public void startgetTemplatesDocuments(

            it.opencontent.nttdata._2012.pi3.GetTemplatesDocuments getTemplatesDocuments34,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getFileWithSignatureAndSignerInfo36
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetFileWithSignatureAndSignerInfoResponse getFileWithSignatureAndSignerInfo(

                        it.opencontent.nttdata._2012.pi3.GetFileWithSignatureAndSignerInfo getFileWithSignatureAndSignerInfo36)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getFileWithSignatureAndSignerInfo36
            
          */
        public void startgetFileWithSignatureAndSignerInfo(

            it.opencontent.nttdata._2012.pi3.GetFileWithSignatureAndSignerInfo getFileWithSignatureAndSignerInfo36,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param createDocumentFromWord38
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.CreateDocumentFromWordResponse createDocumentFromWord(

                        it.opencontent.nttdata._2012.pi3.CreateDocumentFromWord createDocumentFromWord38)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param createDocumentFromWord38
            
          */
        public void startcreateDocumentFromWord(

            it.opencontent.nttdata._2012.pi3.CreateDocumentFromWord createDocumentFromWord38,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getFileWithSignatureOrStamp40
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetFileWithSignatureOrStampResponse getFileWithSignatureOrStamp(

                        it.opencontent.nttdata._2012.pi3.GetFileWithSignatureOrStamp getFileWithSignatureOrStamp40)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getFileWithSignatureOrStamp40
            
          */
        public void startgetFileWithSignatureOrStamp(

            it.opencontent.nttdata._2012.pi3.GetFileWithSignatureOrStamp getFileWithSignatureOrStamp40,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getFileDocumentById42
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetFileDocumentByIdResponse getFileDocumentById(

                        it.opencontent.nttdata._2012.pi3.GetFileDocumentById getFileDocumentById42)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getFileDocumentById42
            
          */
        public void startgetFileDocumentById(

            it.opencontent.nttdata._2012.pi3.GetFileDocumentById getFileDocumentById42,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getDocumentsInProject44
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetDocumentsInProjectResponse getDocumentsInProject(

                        it.opencontent.nttdata._2012.pi3.GetDocumentsInProject getDocumentsInProject44)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getDocumentsInProject44
            
          */
        public void startgetDocumentsInProject(

            it.opencontent.nttdata._2012.pi3.GetDocumentsInProject getDocumentsInProject44,

            final it.opencontent.nttdata._2012.pi3.DocumentsCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        
       //
       }
    