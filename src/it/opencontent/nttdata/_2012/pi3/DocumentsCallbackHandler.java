
/**
 * DocumentsCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */

    package it.opencontent.nttdata._2012.pi3;

    /**
     *  DocumentsCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class DocumentsCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public DocumentsCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public DocumentsCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getDocumentFilters method
            * override this method for handling normal response from getDocumentFilters operation
            */
           public void receiveResultgetDocumentFilters(
                    it.opencontent.nttdata._2012.pi3.GetDocumentFiltersResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDocumentFilters operation
           */
            public void receiveErrorgetDocumentFilters(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getTemplateDoc method
            * override this method for handling normal response from getTemplateDoc operation
            */
           public void receiveResultgetTemplateDoc(
                    it.opencontent.nttdata._2012.pi3.GetTemplateDocResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getTemplateDoc operation
           */
            public void receiveErrorgetTemplateDoc(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for editDocStateDiagram method
            * override this method for handling normal response from editDocStateDiagram operation
            */
           public void receiveResulteditDocStateDiagram(
                    it.opencontent.nttdata._2012.pi3.EditDocStateDiagramResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from editDocStateDiagram operation
           */
            public void receiveErroreditDocStateDiagram(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createDocumentAndAddInProject method
            * override this method for handling normal response from createDocumentAndAddInProject operation
            */
           public void receiveResultcreateDocumentAndAddInProject(
                    it.opencontent.nttdata._2012.pi3.CreateDocumentAndAddInProjectResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createDocumentAndAddInProject operation
           */
            public void receiveErrorcreateDocumentAndAddInProject(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getLinkDocByID method
            * override this method for handling normal response from getLinkDocByID operation
            */
           public void receiveResultgetLinkDocByID(
                    it.opencontent.nttdata._2012.pi3.GetLinkDocByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getLinkDocByID operation
           */
            public void receiveErrorgetLinkDocByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getEnvelopedFileById method
            * override this method for handling normal response from getEnvelopedFileById operation
            */
           public void receiveResultgetEnvelopedFileById(
                    it.opencontent.nttdata._2012.pi3.GetEnvelopedFileByIdResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getEnvelopedFileById operation
           */
            public void receiveErrorgetEnvelopedFileById(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createDocument method
            * override this method for handling normal response from createDocument operation
            */
           public void receiveResultcreateDocument(
                    it.opencontent.nttdata._2012.pi3.CreateDocumentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createDocument operation
           */
            public void receiveErrorcreateDocument(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for sendDocument method
            * override this method for handling normal response from sendDocument operation
            */
           public void receiveResultsendDocument(
                    it.opencontent.nttdata._2012.pi3.SendDocumentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from sendDocument operation
           */
            public void receiveErrorsendDocument(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for editDocument method
            * override this method for handling normal response from editDocument operation
            */
           public void receiveResulteditDocument(
                    it.opencontent.nttdata._2012.pi3.EditDocumentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from editDocument operation
           */
            public void receiveErroreditDocument(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDocument method
            * override this method for handling normal response from getDocument operation
            */
           public void receiveResultgetDocument(
                    it.opencontent.nttdata._2012.pi3.GetDocumentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDocument operation
           */
            public void receiveErrorgetDocument(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for uploadFileToDocument method
            * override this method for handling normal response from uploadFileToDocument operation
            */
           public void receiveResultuploadFileToDocument(
                    it.opencontent.nttdata._2012.pi3.UploadFileToDocumentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from uploadFileToDocument operation
           */
            public void receiveErroruploadFileToDocument(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for followDocument method
            * override this method for handling normal response from followDocument operation
            */
           public void receiveResultfollowDocument(
                    it.opencontent.nttdata._2012.pi3.FollowDocumentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from followDocument operation
           */
            public void receiveErrorfollowDocument(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDocumentStateDiagram method
            * override this method for handling normal response from getDocumentStateDiagram operation
            */
           public void receiveResultgetDocumentStateDiagram(
                    it.opencontent.nttdata._2012.pi3.GetDocumentStateDiagramResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDocumentStateDiagram operation
           */
            public void receiveErrorgetDocumentStateDiagram(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for searchDocuments method
            * override this method for handling normal response from searchDocuments operation
            */
           public void receiveResultsearchDocuments(
                    it.opencontent.nttdata._2012.pi3.SearchDocumentsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from searchDocuments operation
           */
            public void receiveErrorsearchDocuments(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for uploadFileToDocumentFromWord method
            * override this method for handling normal response from uploadFileToDocumentFromWord operation
            */
           public void receiveResultuploadFileToDocumentFromWord(
                    it.opencontent.nttdata._2012.pi3.UploadFileToDocumentFromWordResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from uploadFileToDocumentFromWord operation
           */
            public void receiveErroruploadFileToDocumentFromWord(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getStampAndSignature method
            * override this method for handling normal response from getStampAndSignature operation
            */
           public void receiveResultgetStampAndSignature(
                    it.opencontent.nttdata._2012.pi3.GetStampAndSignatureResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getStampAndSignature operation
           */
            public void receiveErrorgetStampAndSignature(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addDocInProject method
            * override this method for handling normal response from addDocInProject operation
            */
           public void receiveResultaddDocInProject(
                    it.opencontent.nttdata._2012.pi3.AddDocInProjectResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addDocInProject operation
           */
            public void receiveErroraddDocInProject(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getTemplatesDocuments method
            * override this method for handling normal response from getTemplatesDocuments operation
            */
           public void receiveResultgetTemplatesDocuments(
                    it.opencontent.nttdata._2012.pi3.GetTemplatesDocumentsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getTemplatesDocuments operation
           */
            public void receiveErrorgetTemplatesDocuments(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFileWithSignatureAndSignerInfo method
            * override this method for handling normal response from getFileWithSignatureAndSignerInfo operation
            */
           public void receiveResultgetFileWithSignatureAndSignerInfo(
                    it.opencontent.nttdata._2012.pi3.GetFileWithSignatureAndSignerInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFileWithSignatureAndSignerInfo operation
           */
            public void receiveErrorgetFileWithSignatureAndSignerInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createDocumentFromWord method
            * override this method for handling normal response from createDocumentFromWord operation
            */
           public void receiveResultcreateDocumentFromWord(
                    it.opencontent.nttdata._2012.pi3.CreateDocumentFromWordResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createDocumentFromWord operation
           */
            public void receiveErrorcreateDocumentFromWord(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFileWithSignatureOrStamp method
            * override this method for handling normal response from getFileWithSignatureOrStamp operation
            */
           public void receiveResultgetFileWithSignatureOrStamp(
                    it.opencontent.nttdata._2012.pi3.GetFileWithSignatureOrStampResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFileWithSignatureOrStamp operation
           */
            public void receiveErrorgetFileWithSignatureOrStamp(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getFileDocumentById method
            * override this method for handling normal response from getFileDocumentById operation
            */
           public void receiveResultgetFileDocumentById(
                    it.opencontent.nttdata._2012.pi3.GetFileDocumentByIdResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getFileDocumentById operation
           */
            public void receiveErrorgetFileDocumentById(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDocumentsInProject method
            * override this method for handling normal response from getDocumentsInProject operation
            */
           public void receiveResultgetDocumentsInProject(
                    it.opencontent.nttdata._2012.pi3.GetDocumentsInProjectResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDocumentsInProject operation
           */
            public void receiveErrorgetDocumentsInProject(java.lang.Exception e) {
            }
                


    }
    