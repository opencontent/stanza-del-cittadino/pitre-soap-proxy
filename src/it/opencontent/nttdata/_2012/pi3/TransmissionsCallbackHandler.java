
/**
 * TransmissionsCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */

    package it.opencontent.nttdata._2012.pi3;

    /**
     *  TransmissionsCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class TransmissionsCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public TransmissionsCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public TransmissionsCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for executeTransmissionProject method
            * override this method for handling normal response from executeTransmissionProject operation
            */
           public void receiveResultexecuteTransmissionProject(
                    it.opencontent.nttdata._2012.pi3.ExecuteTransmissionProjectResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from executeTransmissionProject operation
           */
            public void receiveErrorexecuteTransmissionProject(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for giveUpRights method
            * override this method for handling normal response from giveUpRights operation
            */
           public void receiveResultgiveUpRights(
                    it.opencontent.nttdata._2012.pi3.GiveUpRightsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from giveUpRights operation
           */
            public void receiveErrorgiveUpRights(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for executeTransmPrjModel method
            * override this method for handling normal response from executeTransmPrjModel operation
            */
           public void receiveResultexecuteTransmPrjModel(
                    it.opencontent.nttdata._2012.pi3.ExecuteTransmPrjModelResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from executeTransmPrjModel operation
           */
            public void receiveErrorexecuteTransmPrjModel(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getTransmissionModel method
            * override this method for handling normal response from getTransmissionModel operation
            */
           public void receiveResultgetTransmissionModel(
                    it.opencontent.nttdata._2012.pi3.GetTransmissionModelResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getTransmissionModel operation
           */
            public void receiveErrorgetTransmissionModel(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getTransmissionModels method
            * override this method for handling normal response from getTransmissionModels operation
            */
           public void receiveResultgetTransmissionModels(
                    it.opencontent.nttdata._2012.pi3.GetTransmissionModelsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getTransmissionModels operation
           */
            public void receiveErrorgetTransmissionModels(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for executeTransmissionDocument method
            * override this method for handling normal response from executeTransmissionDocument operation
            */
           public void receiveResultexecuteTransmissionDocument(
                    it.opencontent.nttdata._2012.pi3.ExecuteTransmissionDocumentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from executeTransmissionDocument operation
           */
            public void receiveErrorexecuteTransmissionDocument(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for executeTransmDocModel method
            * override this method for handling normal response from executeTransmDocModel operation
            */
           public void receiveResultexecuteTransmDocModel(
                    it.opencontent.nttdata._2012.pi3.ExecuteTransmDocModelResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from executeTransmDocModel operation
           */
            public void receiveErrorexecuteTransmDocModel(java.lang.Exception e) {
            }
                


    }
    