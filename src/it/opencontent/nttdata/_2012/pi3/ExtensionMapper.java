
/**
 * ExtensionMapper.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */

        
            package it.opencontent.nttdata._2012.pi3;
        
            /**
            *  ExtensionMapper class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class ExtensionMapper{

          public static java.lang.Object getTypeObject(java.lang.String namespaceURI,
                                                       java.lang.String typeName,
                                                       javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{

              
                  if (
                  "http://schemas.datacontract.org/2004/07/VtDocsWS.Services.Transmissions.ExecuteTransmDocModel".equals(namespaceURI) &&
                  "ExecuteTransmDocModelRequest".equals(typeName)){
                   
                            return  it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_transmissions_executetransmdocmodel.ExecuteTransmDocModelRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.datacontract.org/2004/07/VtDocsWS.Services.Transmissions.ExecuteTransmissionProject".equals(namespaceURI) &&
                  "ExecuteTransmissionProjectResponse".equals(typeName)){
                   
                            return  it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_transmissions_executetransmissionproject.ExecuteTransmissionProjectResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.microsoft.com/2003/10/Serialization/".equals(namespaceURI) &&
                  "char".equals(typeName)){
                   
                            return  it.opencontent.microsoft.schemas._2003._10.serialization._char.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.datacontract.org/2004/07/VtDocsWS.Services.Transmissions.GiveUpRights".equals(namespaceURI) &&
                  "GiveUpRightsRequest".equals(typeName)){
                   
                            return  it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_transmissions_giveuprights.GiveUpRightsRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.microsoft.com/2003/10/Serialization/".equals(namespaceURI) &&
                  "guid".equals(typeName)){
                   
                            return  it.opencontent.microsoft.schemas._2003._10.serialization.Guid.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://nttdata.com/2012/Pi3".equals(namespaceURI) &&
                  "TransmissionModel".equals(typeName)){
                   
                            return  it.opencontent.nttdata._2012.pi3.TransmissionModel.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://nttdata.com/2012/Pi3".equals(namespaceURI) &&
                  "ArrayOfTransmissionModel".equals(typeName)){
                   
                            return  it.opencontent.nttdata._2012.pi3.ArrayOfTransmissionModel.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.datacontract.org/2004/07/VtDocsWS.Services.Transmissions.ExecuteTransmPrjModel".equals(namespaceURI) &&
                  "ExecuteTransmPrjModelResponse".equals(typeName)){
                   
                            return  it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_transmissions_executetransmprjmodel.ExecuteTransmPrjModelResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://nttdata.com/2012/Pi3".equals(namespaceURI) &&
                  "Correspondent".equals(typeName)){
                   
                            return  it.opencontent.nttdata._2012.pi3.Correspondent.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.datacontract.org/2004/07/VtDocsWS.Services".equals(namespaceURI) &&
                  "Response".equals(typeName)){
                   
                            return  it.opencontent.datacontract.schemas._2004._07.vtdocsws_services.Response.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.datacontract.org/2004/07/VtDocsWS.Services.Transmissions.ExecuteTransmissionDocument".equals(namespaceURI) &&
                  "ExecuteTransmissionDocumentResponse".equals(typeName)){
                   
                            return  it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_transmissions_executetransmissiondocument.ExecuteTransmissionDocumentResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.microsoft.com/2003/10/Serialization/".equals(namespaceURI) &&
                  "duration".equals(typeName)){
                   
                            return  it.opencontent.microsoft.schemas._2003._10.serialization.Duration.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.datacontract.org/2004/07/VtDocsWS.Services.Transmissions.GetTransmissionModel".equals(namespaceURI) &&
                  "GetTransmissionModelRequest".equals(typeName)){
                   
                            return  it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_transmissions_gettransmissionmodel.GetTransmissionModelRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://nttdata.com/2012/Pi3".equals(namespaceURI) &&
                  "Register".equals(typeName)){
                   
                            return  it.opencontent.nttdata._2012.pi3.Register.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.datacontract.org/2004/07/VtDocsWS.Services.Transmissions.ExecuteTransmissionDocument".equals(namespaceURI) &&
                  "ExecuteTransmissionDocumentRequest".equals(typeName)){
                   
                            return  it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_transmissions_executetransmissiondocument.ExecuteTransmissionDocumentRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.datacontract.org/2004/07/VtDocsWS.Services.Transmissions.GetTransmissionModels".equals(namespaceURI) &&
                  "GetTransmissionModelsRequest".equals(typeName)){
                   
                            return  it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_transmissions_gettransmissionmodels.GetTransmissionModelsRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.datacontract.org/2004/07/VtDocsWS.Services.Transmissions.GiveUpRights".equals(namespaceURI) &&
                  "GiveUpRightsResponse".equals(typeName)){
                   
                            return  it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_transmissions_giveuprights.GiveUpRightsResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.datacontract.org/2004/07/VtDocsWS.Services".equals(namespaceURI) &&
                  "Request".equals(typeName)){
                   
                            return  it.opencontent.datacontract.schemas._2004._07.vtdocsws_services.Request.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://nttdata.com/2012/Pi3".equals(namespaceURI) &&
                  "ArrayOfRegister".equals(typeName)){
                   
                            return  it.opencontent.nttdata._2012.pi3.ArrayOfRegister.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.datacontract.org/2004/07/VtDocsWS.Services.Transmissions.ExecuteTransmissionProject".equals(namespaceURI) &&
                  "ExecuteTransmissionProjectRequest".equals(typeName)){
                   
                            return  it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_transmissions_executetransmissionproject.ExecuteTransmissionProjectRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.datacontract.org/2004/07/VtDocsWS.Services.Transmissions.GetTransmissionModel".equals(namespaceURI) &&
                  "GetTransmissionModelResponse".equals(typeName)){
                   
                            return  it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_transmissions_gettransmissionmodel.GetTransmissionModelResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.datacontract.org/2004/07/VtDocsWS.Services.Transmissions.ExecuteTransmDocModel".equals(namespaceURI) &&
                  "ExecuteTransmDocModelResponse".equals(typeName)){
                   
                            return  it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_transmissions_executetransmdocmodel.ExecuteTransmDocModelResponse.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.datacontract.org/2004/07/VtDocsWS.Services.Transmissions.ExecuteTransmPrjModel".equals(namespaceURI) &&
                  "ExecuteTransmPrjModelRequest".equals(typeName)){
                   
                            return  it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_transmissions_executetransmprjmodel.ExecuteTransmPrjModelRequest.Factory.parse(reader);
                        

                  }

              
                  if (
                  "http://schemas.datacontract.org/2004/07/VtDocsWS.Services.Transmissions.GetTransmissionModels".equals(namespaceURI) &&
                  "GetTransmissionModelsResponse".equals(typeName)){
                   
                            return  it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_transmissions_gettransmissionmodels.GetTransmissionModelsResponse.Factory.parse(reader);
                        

                  }

              
             throw new org.apache.axis2.databinding.ADBException("Unsupported type " + namespaceURI + " " + typeName);
          }

        }
    