
/**
 * ProjectsCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */

    package it.opencontent.nttdata._2012.pi3;

    /**
     *  ProjectsCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ProjectsCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ProjectsCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ProjectsCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for followProject method
            * override this method for handling normal response from followProject operation
            */
           public void receiveResultfollowProject(
                    it.opencontent.nttdata._2012.pi3.FollowProjectResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from followProject operation
           */
            public void receiveErrorfollowProject(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectStateDiagram method
            * override this method for handling normal response from getProjectStateDiagram operation
            */
           public void receiveResultgetProjectStateDiagram(
                    it.opencontent.nttdata._2012.pi3.GetProjectStateDiagramResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectStateDiagram operation
           */
            public void receiveErrorgetProjectStateDiagram(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for editProject method
            * override this method for handling normal response from editProject operation
            */
           public void receiveResulteditProject(
                    it.opencontent.nttdata._2012.pi3.EditProjectResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from editProject operation
           */
            public void receiveErroreditProject(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectsByDocument method
            * override this method for handling normal response from getProjectsByDocument operation
            */
           public void receiveResultgetProjectsByDocument(
                    it.opencontent.nttdata._2012.pi3.GetProjectsByDocumentResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectsByDocument operation
           */
            public void receiveErrorgetProjectsByDocument(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for openCloseProject method
            * override this method for handling normal response from openCloseProject operation
            */
           public void receiveResultopenCloseProject(
                    it.opencontent.nttdata._2012.pi3.OpenCloseProjectResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from openCloseProject operation
           */
            public void receiveErroropenCloseProject(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getTemplatePrj method
            * override this method for handling normal response from getTemplatePrj operation
            */
           public void receiveResultgetTemplatePrj(
                    it.opencontent.nttdata._2012.pi3.GetTemplatePrjResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getTemplatePrj operation
           */
            public void receiveErrorgetTemplatePrj(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProjectFilters method
            * override this method for handling normal response from getProjectFilters operation
            */
           public void receiveResultgetProjectFilters(
                    it.opencontent.nttdata._2012.pi3.GetProjectFiltersResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProjectFilters operation
           */
            public void receiveErrorgetProjectFilters(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getProject method
            * override this method for handling normal response from getProject operation
            */
           public void receiveResultgetProject(
                    it.opencontent.nttdata._2012.pi3.GetProjectResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getProject operation
           */
            public void receiveErrorgetProject(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for searchProjects method
            * override this method for handling normal response from searchProjects operation
            */
           public void receiveResultsearchProjects(
                    it.opencontent.nttdata._2012.pi3.SearchProjectsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from searchProjects operation
           */
            public void receiveErrorsearchProjects(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createProject method
            * override this method for handling normal response from createProject operation
            */
           public void receiveResultcreateProject(
                    it.opencontent.nttdata._2012.pi3.CreateProjectResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createProject operation
           */
            public void receiveErrorcreateProject(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getTemplatesProjects method
            * override this method for handling normal response from getTemplatesProjects operation
            */
           public void receiveResultgetTemplatesProjects(
                    it.opencontent.nttdata._2012.pi3.GetTemplatesProjectsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getTemplatesProjects operation
           */
            public void receiveErrorgetTemplatesProjects(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getLinkPrjByID method
            * override this method for handling normal response from getLinkPrjByID operation
            */
           public void receiveResultgetLinkPrjByID(
                    it.opencontent.nttdata._2012.pi3.GetLinkPrjByIDResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getLinkPrjByID operation
           */
            public void receiveErrorgetLinkPrjByID(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for editPrjStateDiagram method
            * override this method for handling normal response from editPrjStateDiagram operation
            */
           public void receiveResulteditPrjStateDiagram(
                    it.opencontent.nttdata._2012.pi3.EditPrjStateDiagramResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from editPrjStateDiagram operation
           */
            public void receiveErroreditPrjStateDiagram(java.lang.Exception e) {
            }
                


    }
    