

/**
 * AddressBook.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */

    package it.opencontent.nttdata._2012.pi3;

    /*
     *  AddressBook java interface
     */

    public interface AddressBook {
          

        /**
          * Auto generated method signature
          * 
                    * @param getCorrespondent0
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetCorrespondentResponse getCorrespondent(

                        it.opencontent.nttdata._2012.pi3.GetCorrespondent getCorrespondent0)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getCorrespondent0
            
          */
        public void startgetCorrespondent(

            it.opencontent.nttdata._2012.pi3.GetCorrespondent getCorrespondent0,

            final it.opencontent.nttdata._2012.pi3.AddressBookCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param addCorrespondent2
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.AddCorrespondentResponse addCorrespondent(

                        it.opencontent.nttdata._2012.pi3.AddCorrespondent addCorrespondent2)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param addCorrespondent2
            
          */
        public void startaddCorrespondent(

            it.opencontent.nttdata._2012.pi3.AddCorrespondent addCorrespondent2,

            final it.opencontent.nttdata._2012.pi3.AddressBookCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param searchUsers4
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.SearchUsersResponse searchUsers(

                        it.opencontent.nttdata._2012.pi3.SearchUsers searchUsers4)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param searchUsers4
            
          */
        public void startsearchUsers(

            it.opencontent.nttdata._2012.pi3.SearchUsers searchUsers4,

            final it.opencontent.nttdata._2012.pi3.AddressBookCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getCorrespondentFilters6
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetCorrespondentFiltersResponse getCorrespondentFilters(

                        it.opencontent.nttdata._2012.pi3.GetCorrespondentFilters getCorrespondentFilters6)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getCorrespondentFilters6
            
          */
        public void startgetCorrespondentFilters(

            it.opencontent.nttdata._2012.pi3.GetCorrespondentFilters getCorrespondentFilters6,

            final it.opencontent.nttdata._2012.pi3.AddressBookCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param deleteCorrespondent8
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.DeleteCorrespondentResponse deleteCorrespondent(

                        it.opencontent.nttdata._2012.pi3.DeleteCorrespondent deleteCorrespondent8)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param deleteCorrespondent8
            
          */
        public void startdeleteCorrespondent(

            it.opencontent.nttdata._2012.pi3.DeleteCorrespondent deleteCorrespondent8,

            final it.opencontent.nttdata._2012.pi3.AddressBookCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param editCorrespondent10
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.EditCorrespondentResponse editCorrespondent(

                        it.opencontent.nttdata._2012.pi3.EditCorrespondent editCorrespondent10)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param editCorrespondent10
            
          */
        public void starteditCorrespondent(

            it.opencontent.nttdata._2012.pi3.EditCorrespondent editCorrespondent10,

            final it.opencontent.nttdata._2012.pi3.AddressBookCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getUserFilters12
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetUserFiltersResponse getUserFilters(

                        it.opencontent.nttdata._2012.pi3.GetUserFilters getUserFilters12)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getUserFilters12
            
          */
        public void startgetUserFilters(

            it.opencontent.nttdata._2012.pi3.GetUserFilters getUserFilters12,

            final it.opencontent.nttdata._2012.pi3.AddressBookCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getOpportunityList14
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetOpportunityListResponse getOpportunityList(

                        it.opencontent.nttdata._2012.pi3.GetOpportunityList getOpportunityList14)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getOpportunityList14
            
          */
        public void startgetOpportunityList(

            it.opencontent.nttdata._2012.pi3.GetOpportunityList getOpportunityList14,

            final it.opencontent.nttdata._2012.pi3.AddressBookCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param searchCorrespondents16
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.SearchCorrespondentsResponse searchCorrespondents(

                        it.opencontent.nttdata._2012.pi3.SearchCorrespondents searchCorrespondents16)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param searchCorrespondents16
            
          */
        public void startsearchCorrespondents(

            it.opencontent.nttdata._2012.pi3.SearchCorrespondents searchCorrespondents16,

            final it.opencontent.nttdata._2012.pi3.AddressBookCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        
       //
       }
    