
/**
 * Document.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:09:26 BST)
 */

            
                package it.opencontent.nttdata._2012.pi3;
            

            /**
            *  Document bean class
            */
            @SuppressWarnings({"unchecked","unused"})
        
        public  class Document
        implements org.apache.axis2.databinding.ADBBean{
        /* This type was generated from the piece of schema that had
                name = Document
                Namespace URI = http://nttdata.com/2012/Pi3
                Namespace Prefix = ns2
                */
            

                        /**
                        * field for Annulled
                        */

                        
                                    protected boolean localAnnulled ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAnnulledTracker = false ;

                           public boolean isAnnulledSpecified(){
                               return localAnnulledTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getAnnulled(){
                               return localAnnulled;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Annulled
                               */
                               public void setAnnulled(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localAnnulledTracker =
                                       true;
                                   
                                            this.localAnnulled=param;
                                       

                               }
                            

                        /**
                        * field for ArrivalDate
                        */

                        
                                    protected java.lang.String localArrivalDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localArrivalDateTracker = false ;

                           public boolean isArrivalDateSpecified(){
                               return localArrivalDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getArrivalDate(){
                               return localArrivalDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ArrivalDate
                               */
                               public void setArrivalDate(java.lang.String param){
                            localArrivalDateTracker = true;
                                   
                                            this.localArrivalDate=param;
                                       

                               }
                            

                        /**
                        * field for Attachments
                        */

                        
                                    protected it.opencontent.nttdata._2012.pi3.ArrayOfFile localAttachments ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localAttachmentsTracker = false ;

                           public boolean isAttachmentsSpecified(){
                               return localAttachmentsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return it.opencontent.nttdata._2012.pi3.ArrayOfFile
                           */
                           public  it.opencontent.nttdata._2012.pi3.ArrayOfFile getAttachments(){
                               return localAttachments;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Attachments
                               */
                               public void setAttachments(it.opencontent.nttdata._2012.pi3.ArrayOfFile param){
                            localAttachmentsTracker = true;
                                   
                                            this.localAttachments=param;
                                       

                               }
                            

                        /**
                        * field for ConsolidationState
                        */

                        
                                    protected java.lang.String localConsolidationState ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localConsolidationStateTracker = false ;

                           public boolean isConsolidationStateSpecified(){
                               return localConsolidationStateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getConsolidationState(){
                               return localConsolidationState;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ConsolidationState
                               */
                               public void setConsolidationState(java.lang.String param){
                            localConsolidationStateTracker = true;
                                   
                                            this.localConsolidationState=param;
                                       

                               }
                            

                        /**
                        * field for CreationDate
                        */

                        
                                    protected java.lang.String localCreationDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localCreationDateTracker = false ;

                           public boolean isCreationDateSpecified(){
                               return localCreationDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getCreationDate(){
                               return localCreationDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param CreationDate
                               */
                               public void setCreationDate(java.lang.String param){
                            localCreationDateTracker = true;
                                   
                                            this.localCreationDate=param;
                                       

                               }
                            

                        /**
                        * field for DataProtocolSender
                        */

                        
                                    protected java.lang.String localDataProtocolSender ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDataProtocolSenderTracker = false ;

                           public boolean isDataProtocolSenderSpecified(){
                               return localDataProtocolSenderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDataProtocolSender(){
                               return localDataProtocolSender;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DataProtocolSender
                               */
                               public void setDataProtocolSender(java.lang.String param){
                            localDataProtocolSenderTracker = true;
                                   
                                            this.localDataProtocolSender=param;
                                       

                               }
                            

                        /**
                        * field for DocNumber
                        */

                        
                                    protected java.lang.String localDocNumber ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDocNumberTracker = false ;

                           public boolean isDocNumberSpecified(){
                               return localDocNumberTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDocNumber(){
                               return localDocNumber;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DocNumber
                               */
                               public void setDocNumber(java.lang.String param){
                            localDocNumberTracker = true;
                                   
                                            this.localDocNumber=param;
                                       

                               }
                            

                        /**
                        * field for DocumentType
                        */

                        
                                    protected java.lang.String localDocumentType ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localDocumentTypeTracker = false ;

                           public boolean isDocumentTypeSpecified(){
                               return localDocumentTypeTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getDocumentType(){
                               return localDocumentType;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param DocumentType
                               */
                               public void setDocumentType(java.lang.String param){
                            localDocumentTypeTracker = true;
                                   
                                            this.localDocumentType=param;
                                       

                               }
                            

                        /**
                        * field for Id
                        */

                        
                                    protected java.lang.String localId ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIdTracker = false ;

                           public boolean isIdSpecified(){
                               return localIdTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getId(){
                               return localId;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Id
                               */
                               public void setId(java.lang.String param){
                            localIdTracker = true;
                                   
                                            this.localId=param;
                                       

                               }
                            

                        /**
                        * field for IdParent
                        */

                        
                                    protected java.lang.String localIdParent ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIdParentTracker = false ;

                           public boolean isIdParentSpecified(){
                               return localIdParentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getIdParent(){
                               return localIdParent;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IdParent
                               */
                               public void setIdParent(java.lang.String param){
                            localIdParentTracker = true;
                                   
                                            this.localIdParent=param;
                                       

                               }
                            

                        /**
                        * field for InBasket
                        */

                        
                                    protected boolean localInBasket ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localInBasketTracker = false ;

                           public boolean isInBasketSpecified(){
                               return localInBasketTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getInBasket(){
                               return localInBasket;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param InBasket
                               */
                               public void setInBasket(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localInBasketTracker =
                                       true;
                                   
                                            this.localInBasket=param;
                                       

                               }
                            

                        /**
                        * field for IsAttachments
                        */

                        
                                    protected boolean localIsAttachments ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localIsAttachmentsTracker = false ;

                           public boolean isIsAttachmentsSpecified(){
                               return localIsAttachmentsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getIsAttachments(){
                               return localIsAttachments;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param IsAttachments
                               */
                               public void setIsAttachments(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localIsAttachmentsTracker =
                                       true;
                                   
                                            this.localIsAttachments=param;
                                       

                               }
                            

                        /**
                        * field for MainDocument
                        */

                        
                                    protected it.opencontent.nttdata._2012.pi3.File localMainDocument ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMainDocumentTracker = false ;

                           public boolean isMainDocumentSpecified(){
                               return localMainDocumentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return it.opencontent.nttdata._2012.pi3.File
                           */
                           public  it.opencontent.nttdata._2012.pi3.File getMainDocument(){
                               return localMainDocument;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MainDocument
                               */
                               public void setMainDocument(it.opencontent.nttdata._2012.pi3.File param){
                            localMainDocumentTracker = true;
                                   
                                            this.localMainDocument=param;
                                       

                               }
                            

                        /**
                        * field for MeansOfSending
                        */

                        
                                    protected java.lang.String localMeansOfSending ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMeansOfSendingTracker = false ;

                           public boolean isMeansOfSendingSpecified(){
                               return localMeansOfSendingTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getMeansOfSending(){
                               return localMeansOfSending;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MeansOfSending
                               */
                               public void setMeansOfSending(java.lang.String param){
                            localMeansOfSendingTracker = true;
                                   
                                            this.localMeansOfSending=param;
                                       

                               }
                            

                        /**
                        * field for MultipleSenders
                        */

                        
                                    protected it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent localMultipleSenders ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localMultipleSendersTracker = false ;

                           public boolean isMultipleSendersSpecified(){
                               return localMultipleSendersTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent
                           */
                           public  it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent getMultipleSenders(){
                               return localMultipleSenders;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param MultipleSenders
                               */
                               public void setMultipleSenders(it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent param){
                            localMultipleSendersTracker = true;
                                   
                                            this.localMultipleSenders=param;
                                       

                               }
                            

                        /**
                        * field for Note
                        */

                        
                                    protected it.opencontent.nttdata._2012.pi3.ArrayOfNote localNote ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localNoteTracker = false ;

                           public boolean isNoteSpecified(){
                               return localNoteTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return it.opencontent.nttdata._2012.pi3.ArrayOfNote
                           */
                           public  it.opencontent.nttdata._2012.pi3.ArrayOfNote getNote(){
                               return localNote;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Note
                               */
                               public void setNote(it.opencontent.nttdata._2012.pi3.ArrayOfNote param){
                            localNoteTracker = true;
                                   
                                            this.localNote=param;
                                       

                               }
                            

                        /**
                        * field for Object
                        */

                        
                                    protected java.lang.String localObject ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localObjectTracker = false ;

                           public boolean isObjectSpecified(){
                               return localObjectTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getObject(){
                               return localObject;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Object
                               */
                               public void setObject(java.lang.String param){
                            localObjectTracker = true;
                                   
                                            this.localObject=param;
                                       

                               }
                            

                        /**
                        * field for PersonalDocument
                        */

                        
                                    protected boolean localPersonalDocument ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPersonalDocumentTracker = false ;

                           public boolean isPersonalDocumentSpecified(){
                               return localPersonalDocumentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getPersonalDocument(){
                               return localPersonalDocument;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PersonalDocument
                               */
                               public void setPersonalDocument(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localPersonalDocumentTracker =
                                       true;
                                   
                                            this.localPersonalDocument=param;
                                       

                               }
                            

                        /**
                        * field for Predisposed
                        */

                        
                                    protected boolean localPredisposed ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPredisposedTracker = false ;

                           public boolean isPredisposedSpecified(){
                               return localPredisposedTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getPredisposed(){
                               return localPredisposed;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Predisposed
                               */
                               public void setPredisposed(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localPredisposedTracker =
                                       true;
                                   
                                            this.localPredisposed=param;
                                       

                               }
                            

                        /**
                        * field for PrivateDocument
                        */

                        
                                    protected boolean localPrivateDocument ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localPrivateDocumentTracker = false ;

                           public boolean isPrivateDocumentSpecified(){
                               return localPrivateDocumentTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return boolean
                           */
                           public  boolean getPrivateDocument(){
                               return localPrivateDocument;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param PrivateDocument
                               */
                               public void setPrivateDocument(boolean param){
                            
                                       // setting primitive attribute tracker to true
                                       localPrivateDocumentTracker =
                                       true;
                                   
                                            this.localPrivateDocument=param;
                                       

                               }
                            

                        /**
                        * field for ProtocolDate
                        */

                        
                                    protected java.lang.String localProtocolDate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProtocolDateTracker = false ;

                           public boolean isProtocolDateSpecified(){
                               return localProtocolDateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getProtocolDate(){
                               return localProtocolDate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ProtocolDate
                               */
                               public void setProtocolDate(java.lang.String param){
                            localProtocolDateTracker = true;
                                   
                                            this.localProtocolDate=param;
                                       

                               }
                            

                        /**
                        * field for ProtocolSender
                        */

                        
                                    protected java.lang.String localProtocolSender ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localProtocolSenderTracker = false ;

                           public boolean isProtocolSenderSpecified(){
                               return localProtocolSenderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getProtocolSender(){
                               return localProtocolSender;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param ProtocolSender
                               */
                               public void setProtocolSender(java.lang.String param){
                            localProtocolSenderTracker = true;
                                   
                                            this.localProtocolSender=param;
                                       

                               }
                            

                        /**
                        * field for Recipients
                        */

                        
                                    protected it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent localRecipients ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecipientsTracker = false ;

                           public boolean isRecipientsSpecified(){
                               return localRecipientsTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent
                           */
                           public  it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent getRecipients(){
                               return localRecipients;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Recipients
                               */
                               public void setRecipients(it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent param){
                            localRecipientsTracker = true;
                                   
                                            this.localRecipients=param;
                                       

                               }
                            

                        /**
                        * field for RecipientsCC
                        */

                        
                                    protected it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent localRecipientsCC ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRecipientsCCTracker = false ;

                           public boolean isRecipientsCCSpecified(){
                               return localRecipientsCCTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent
                           */
                           public  it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent getRecipientsCC(){
                               return localRecipientsCC;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param RecipientsCC
                               */
                               public void setRecipientsCC(it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent param){
                            localRecipientsCCTracker = true;
                                   
                                            this.localRecipientsCC=param;
                                       

                               }
                            

                        /**
                        * field for Register
                        */

                        
                                    protected it.opencontent.nttdata._2012.pi3.Register localRegister ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localRegisterTracker = false ;

                           public boolean isRegisterSpecified(){
                               return localRegisterTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return it.opencontent.nttdata._2012.pi3.Register
                           */
                           public  it.opencontent.nttdata._2012.pi3.Register getRegister(){
                               return localRegister;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Register
                               */
                               public void setRegister(it.opencontent.nttdata._2012.pi3.Register param){
                            localRegisterTracker = true;
                                   
                                            this.localRegister=param;
                                       

                               }
                            

                        /**
                        * field for Sender
                        */

                        
                                    protected it.opencontent.nttdata._2012.pi3.Correspondent localSender ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSenderTracker = false ;

                           public boolean isSenderSpecified(){
                               return localSenderTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return it.opencontent.nttdata._2012.pi3.Correspondent
                           */
                           public  it.opencontent.nttdata._2012.pi3.Correspondent getSender(){
                               return localSender;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Sender
                               */
                               public void setSender(it.opencontent.nttdata._2012.pi3.Correspondent param){
                            localSenderTracker = true;
                                   
                                            this.localSender=param;
                                       

                               }
                            

                        /**
                        * field for Signature
                        */

                        
                                    protected java.lang.String localSignature ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localSignatureTracker = false ;

                           public boolean isSignatureSpecified(){
                               return localSignatureTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return java.lang.String
                           */
                           public  java.lang.String getSignature(){
                               return localSignature;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Signature
                               */
                               public void setSignature(java.lang.String param){
                            localSignatureTracker = true;
                                   
                                            this.localSignature=param;
                                       

                               }
                            

                        /**
                        * field for Template
                        */

                        
                                    protected it.opencontent.nttdata._2012.pi3.Template localTemplate ;
                                
                           /*  This tracker boolean wil be used to detect whether the user called the set method
                          *   for this attribute. It will be used to determine whether to include this field
                           *   in the serialized XML
                           */
                           protected boolean localTemplateTracker = false ;

                           public boolean isTemplateSpecified(){
                               return localTemplateTracker;
                           }

                           

                           /**
                           * Auto generated getter method
                           * @return it.opencontent.nttdata._2012.pi3.Template
                           */
                           public  it.opencontent.nttdata._2012.pi3.Template getTemplate(){
                               return localTemplate;
                           }

                           
                        
                            /**
                               * Auto generated setter method
                               * @param param Template
                               */
                               public void setTemplate(it.opencontent.nttdata._2012.pi3.Template param){
                            localTemplateTracker = true;
                                   
                                            this.localTemplate=param;
                                       

                               }
                            

     
     
        /**
        *
        * @param parentQName
        * @param factory
        * @return org.apache.axiom.om.OMElement
        */
       @Override
	public org.apache.axiom.om.OMElement getOMElement (
               final javax.xml.namespace.QName parentQName,
               final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException{


        
               return factory.createOMElement(new org.apache.axis2.databinding.ADBDataSource(this,parentQName));
            
        }

         @Override
		public void serialize(final javax.xml.namespace.QName parentQName,
                                       javax.xml.stream.XMLStreamWriter xmlWriter)
                                throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
                           serialize(parentQName,xmlWriter,false);
         }

         @Override
		public void serialize(final javax.xml.namespace.QName parentQName,
                               javax.xml.stream.XMLStreamWriter xmlWriter,
                               boolean serializeType)
            throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException{
            
                


                java.lang.String prefix = null;
                java.lang.String namespace = null;
                

                    prefix = parentQName.getPrefix();
                    namespace = parentQName.getNamespaceURI();
                    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
                
                  if (serializeType){
               

                   java.lang.String namespacePrefix = registerPrefix(xmlWriter,"http://nttdata.com/2012/Pi3");
                   if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)){
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           namespacePrefix+":Document",
                           xmlWriter);
                   } else {
                       writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","type",
                           "Document",
                           xmlWriter);
                   }

               
                   }
                if (localAnnulledTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "Annulled", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Annulled cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localAnnulled));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localArrivalDateTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "ArrivalDate", xmlWriter);
                             

                                          if (localArrivalDate==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localArrivalDate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localAttachmentsTracker){
                                    if (localAttachments==null){

                                        writeStartElement(null, "http://nttdata.com/2012/Pi3", "Attachments", xmlWriter);

                                       // write the nil attribute
                                      writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                      xmlWriter.writeEndElement();
                                    }else{
                                     localAttachments.serialize(new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Attachments"),
                                        xmlWriter);
                                    }
                                } if (localConsolidationStateTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "ConsolidationState", xmlWriter);
                             

                                          if (localConsolidationState==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localConsolidationState);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localCreationDateTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "CreationDate", xmlWriter);
                             

                                          if (localCreationDate==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localCreationDate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDataProtocolSenderTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "DataProtocolSender", xmlWriter);
                             

                                          if (localDataProtocolSender==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDataProtocolSender);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDocNumberTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "DocNumber", xmlWriter);
                             

                                          if (localDocNumber==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDocNumber);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localDocumentTypeTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "DocumentType", xmlWriter);
                             

                                          if (localDocumentType==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localDocumentType);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIdTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "Id", xmlWriter);
                             

                                          if (localId==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localId);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIdParentTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "IdParent", xmlWriter);
                             

                                          if (localIdParent==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localIdParent);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localInBasketTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "InBasket", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("InBasket cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localInBasket));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localIsAttachmentsTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "IsAttachments", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("IsAttachments cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localIsAttachments));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMainDocumentTracker){
                                    if (localMainDocument==null){

                                        writeStartElement(null, "http://nttdata.com/2012/Pi3", "MainDocument", xmlWriter);

                                       // write the nil attribute
                                      writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                      xmlWriter.writeEndElement();
                                    }else{
                                     localMainDocument.serialize(new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","MainDocument"),
                                        xmlWriter);
                                    }
                                } if (localMeansOfSendingTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "MeansOfSending", xmlWriter);
                             

                                          if (localMeansOfSending==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localMeansOfSending);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localMultipleSendersTracker){
                                    if (localMultipleSenders==null){

                                        writeStartElement(null, "http://nttdata.com/2012/Pi3", "MultipleSenders", xmlWriter);

                                       // write the nil attribute
                                      writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                      xmlWriter.writeEndElement();
                                    }else{
                                     localMultipleSenders.serialize(new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","MultipleSenders"),
                                        xmlWriter);
                                    }
                                } if (localNoteTracker){
                                    if (localNote==null){

                                        writeStartElement(null, "http://nttdata.com/2012/Pi3", "Note", xmlWriter);

                                       // write the nil attribute
                                      writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                      xmlWriter.writeEndElement();
                                    }else{
                                     localNote.serialize(new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Note"),
                                        xmlWriter);
                                    }
                                } if (localObjectTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "Object", xmlWriter);
                             

                                          if (localObject==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localObject);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPersonalDocumentTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "PersonalDocument", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("PersonalDocument cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPersonalDocument));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPredisposedTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "Predisposed", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("Predisposed cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPredisposed));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localPrivateDocumentTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "PrivateDocument", xmlWriter);
                             
                                               if (false) {
                                           
                                                         throw new org.apache.axis2.databinding.ADBException("PrivateDocument cannot be null!!");
                                                      
                                               } else {
                                                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localPrivateDocument));
                                               }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localProtocolDateTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "ProtocolDate", xmlWriter);
                             

                                          if (localProtocolDate==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localProtocolDate);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localProtocolSenderTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "ProtocolSender", xmlWriter);
                             

                                          if (localProtocolSender==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localProtocolSender);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localRecipientsTracker){
                                    if (localRecipients==null){

                                        writeStartElement(null, "http://nttdata.com/2012/Pi3", "Recipients", xmlWriter);

                                       // write the nil attribute
                                      writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                      xmlWriter.writeEndElement();
                                    }else{
                                     localRecipients.serialize(new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Recipients"),
                                        xmlWriter);
                                    }
                                } if (localRecipientsCCTracker){
                                    if (localRecipientsCC==null){

                                        writeStartElement(null, "http://nttdata.com/2012/Pi3", "RecipientsCC", xmlWriter);

                                       // write the nil attribute
                                      writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                      xmlWriter.writeEndElement();
                                    }else{
                                     localRecipientsCC.serialize(new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","RecipientsCC"),
                                        xmlWriter);
                                    }
                                } if (localRegisterTracker){
                                    if (localRegister==null){

                                        writeStartElement(null, "http://nttdata.com/2012/Pi3", "Register", xmlWriter);

                                       // write the nil attribute
                                      writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                      xmlWriter.writeEndElement();
                                    }else{
                                     localRegister.serialize(new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Register"),
                                        xmlWriter);
                                    }
                                } if (localSenderTracker){
                                    if (localSender==null){

                                        writeStartElement(null, "http://nttdata.com/2012/Pi3", "Sender", xmlWriter);

                                       // write the nil attribute
                                      writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                      xmlWriter.writeEndElement();
                                    }else{
                                     localSender.serialize(new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Sender"),
                                        xmlWriter);
                                    }
                                } if (localSignatureTracker){
                                    namespace = "http://nttdata.com/2012/Pi3";
                                    writeStartElement(null, namespace, "Signature", xmlWriter);
                             

                                          if (localSignature==null){
                                              // write the nil attribute
                                              
                                                     writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                                  
                                          }else{

                                        
                                                   xmlWriter.writeCharacters(localSignature);
                                            
                                          }
                                    
                                   xmlWriter.writeEndElement();
                             } if (localTemplateTracker){
                                    if (localTemplate==null){

                                        writeStartElement(null, "http://nttdata.com/2012/Pi3", "Template", xmlWriter);

                                       // write the nil attribute
                                      writeAttribute("xsi","http://www.w3.org/2001/XMLSchema-instance","nil","1",xmlWriter);
                                      xmlWriter.writeEndElement();
                                    }else{
                                     localTemplate.serialize(new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Template"),
                                        xmlWriter);
                                    }
                                }
                    xmlWriter.writeEndElement();
               

        }

        private static java.lang.String generatePrefix(java.lang.String namespace) {
            if(namespace.equals("http://nttdata.com/2012/Pi3")){
                return "ns2";
            }
            return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
        }

        /**
         * Utility method to write an element start tag.
         */
        private void writeStartElement(java.lang.String prefix, java.lang.String namespace, java.lang.String localPart,
                                       javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeStartElement(writerPrefix, localPart, namespace);
            } else {
                if (namespace.length() == 0) {
                    prefix = "";
                } else if (prefix == null) {
                    prefix = generatePrefix(namespace);
                }

                xmlWriter.writeStartElement(prefix, localPart, namespace);
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
        }
        
        /**
         * Util method to write an attribute with the ns prefix
         */
        private void writeAttribute(java.lang.String prefix,java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            java.lang.String writerPrefix = xmlWriter.getPrefix(namespace);
            if (writerPrefix != null) {
                xmlWriter.writeAttribute(writerPrefix, namespace,attName,attValue);
            } else {
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
                xmlWriter.writeAttribute(prefix, namespace,attName,attValue);
            }
        }

        /**
         * Util method to write an attribute without the ns prefix
         */
        private void writeAttribute(java.lang.String namespace,java.lang.String attName,
                                    java.lang.String attValue,javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException{
            if (namespace.equals("")) {
                xmlWriter.writeAttribute(attName,attValue);
            } else {
                xmlWriter.writeAttribute(registerPrefix(xmlWriter, namespace), namespace,attName,attValue);
            }
        }


           /**
             * Util method to write an attribute without the ns prefix
             */
            private void writeQNameAttribute(java.lang.String namespace, java.lang.String attName,
                                             javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

                java.lang.String attributeNamespace = qname.getNamespaceURI();
                java.lang.String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
                if (attributePrefix == null) {
                    attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
                }
                java.lang.String attributeValue;
                if (attributePrefix.trim().length() > 0) {
                    attributeValue = attributePrefix + ":" + qname.getLocalPart();
                } else {
                    attributeValue = qname.getLocalPart();
                }

                if (namespace.equals("")) {
                    xmlWriter.writeAttribute(attName, attributeValue);
                } else {
                    registerPrefix(xmlWriter, namespace);
                    xmlWriter.writeAttribute(attributePrefix, namespace, attName, attributeValue);
                }
            }
        /**
         *  method to handle Qnames
         */

        private void writeQName(javax.xml.namespace.QName qname,
                                javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
            java.lang.String namespaceURI = qname.getNamespaceURI();
            if (namespaceURI != null) {
                java.lang.String prefix = xmlWriter.getPrefix(namespaceURI);
                if (prefix == null) {
                    prefix = generatePrefix(namespaceURI);
                    xmlWriter.writeNamespace(prefix, namespaceURI);
                    xmlWriter.setPrefix(prefix,namespaceURI);
                }

                if (prefix.trim().length() > 0){
                    xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                } else {
                    // i.e this is the default namespace
                    xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
                }

            } else {
                xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
            }
        }

        private void writeQNames(javax.xml.namespace.QName[] qnames,
                                 javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {

            if (qnames != null) {
                // we have to store this data until last moment since it is not possible to write any
                // namespace data after writing the charactor data
                java.lang.StringBuffer stringToWrite = new java.lang.StringBuffer();
                java.lang.String namespaceURI = null;
                java.lang.String prefix = null;

                for (int i = 0; i < qnames.length; i++) {
                    if (i > 0) {
                        stringToWrite.append(" ");
                    }
                    namespaceURI = qnames[i].getNamespaceURI();
                    if (namespaceURI != null) {
                        prefix = xmlWriter.getPrefix(namespaceURI);
                        if ((prefix == null) || (prefix.length() == 0)) {
                            prefix = generatePrefix(namespaceURI);
                            xmlWriter.writeNamespace(prefix, namespaceURI);
                            xmlWriter.setPrefix(prefix,namespaceURI);
                        }

                        if (prefix.trim().length() > 0){
                            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        } else {
                            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                        }
                    } else {
                        stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
                    }
                }
                xmlWriter.writeCharacters(stringToWrite.toString());
            }

        }


        /**
         * Register a namespace prefix
         */
        private java.lang.String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, java.lang.String namespace) throws javax.xml.stream.XMLStreamException {
            java.lang.String prefix = xmlWriter.getPrefix(namespace);
            if (prefix == null) {
                prefix = generatePrefix(namespace);
                javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
                while (true) {
                    java.lang.String uri = nsContext.getNamespaceURI(prefix);
                    if (uri == null || uri.length() == 0) {
                        break;
                    }
                    prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
                }
                xmlWriter.writeNamespace(prefix, namespace);
                xmlWriter.setPrefix(prefix, namespace);
            }
            return prefix;
        }


  

     /**
      *  Factory class that keeps the parse method
      */
    public static class Factory{
        private static org.apache.commons.logging.Log log = org.apache.commons.logging.LogFactory.getLog(Factory.class);

        
        

        /**
        * static method to create the object
        * Precondition:  If this object is an element, the current or next start element starts this object and any intervening reader events are ignorable
        *                If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
        * Postcondition: If this object is an element, the reader is positioned at its end element
        *                If this object is a complex type, the reader is positioned at the end element of its outer element
        */
        public static Document parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception{
            Document object =
                new Document();

            int event;
            javax.xml.namespace.QName currentQName = null;
            java.lang.String nillableValue = null;
            java.lang.String prefix ="";
            java.lang.String namespaceuri ="";
            try {
                
                while (!reader.isStartElement() && !reader.isEndElement())
                    reader.next();

                currentQName = reader.getName();
                
                if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","type")!=null){
                  java.lang.String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance",
                        "type");
                  if (fullTypeName!=null){
                    java.lang.String nsPrefix = null;
                    if (fullTypeName.indexOf(":") > -1){
                        nsPrefix = fullTypeName.substring(0,fullTypeName.indexOf(":"));
                    }
                    nsPrefix = nsPrefix==null?"":nsPrefix;

                    java.lang.String type = fullTypeName.substring(fullTypeName.indexOf(":")+1);
                    
                            if (!"Document".equals(type)){
                                //find namespace for the prefix
                                java.lang.String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
                                return (Document)it.opencontent.nttdata._2012.pi3.ExtensionMapper.getTypeObject(
                                     nsUri,type,reader);
                              }
                        

                  }
                

                }

                

                
                // Note all attributes that were handled. Used to differ normal attributes
                // from anyAttributes.
                java.util.Vector handledAttributes = new java.util.Vector();
                

                
                    
                    reader.next();
                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Annulled").equals(reader.getName()) || new javax.xml.namespace.QName("","Annulled").equals(reader.getName()) ){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"Annulled" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setAnnulled(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","ArrivalDate").equals(reader.getName()) || new javax.xml.namespace.QName("","ArrivalDate").equals(reader.getName()) ){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setArrivalDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Attachments").equals(reader.getName()) || new javax.xml.namespace.QName("","Attachments").equals(reader.getName()) ){
                                
                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                          object.setAttachments(null);
                                          reader.next();
                                            
                                            reader.next();
                                          
                                      }else{
                                    
                                                object.setAttachments(it.opencontent.nttdata._2012.pi3.ArrayOfFile.Factory.parse(reader));
                                              
                                        reader.next();
                                    }
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","ConsolidationState").equals(reader.getName()) || new javax.xml.namespace.QName("","ConsolidationState").equals(reader.getName()) ){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setConsolidationState(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","CreationDate").equals(reader.getName()) || new javax.xml.namespace.QName("","CreationDate").equals(reader.getName()) ){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setCreationDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","DataProtocolSender").equals(reader.getName()) || new javax.xml.namespace.QName("","DataProtocolSender").equals(reader.getName()) ){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDataProtocolSender(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","DocNumber").equals(reader.getName()) || new javax.xml.namespace.QName("","DocNumber").equals(reader.getName()) ){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDocNumber(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","DocumentType").equals(reader.getName()) || new javax.xml.namespace.QName("","DocumentType").equals(reader.getName()) ){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setDocumentType(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Id").equals(reader.getName()) || new javax.xml.namespace.QName("","Id").equals(reader.getName()) ){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setId(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","IdParent").equals(reader.getName()) || new javax.xml.namespace.QName("","IdParent").equals(reader.getName()) ){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIdParent(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","InBasket").equals(reader.getName()) || new javax.xml.namespace.QName("","InBasket").equals(reader.getName()) ){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"InBasket" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setInBasket(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","IsAttachments").equals(reader.getName()) || new javax.xml.namespace.QName("","IsAttachments").equals(reader.getName()) ){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"IsAttachments" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setIsAttachments(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","MainDocument").equals(reader.getName()) || new javax.xml.namespace.QName("","MainDocument").equals(reader.getName()) ){
                                
                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                          object.setMainDocument(null);
                                          reader.next();
                                            
                                            reader.next();
                                          
                                      }else{
                                    
                                                object.setMainDocument(it.opencontent.nttdata._2012.pi3.File.Factory.parse(reader));
                                              
                                        reader.next();
                                    }
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","MeansOfSending").equals(reader.getName()) || new javax.xml.namespace.QName("","MeansOfSending").equals(reader.getName()) ){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setMeansOfSending(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","MultipleSenders").equals(reader.getName()) || new javax.xml.namespace.QName("","MultipleSenders").equals(reader.getName()) ){
                                
                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                          object.setMultipleSenders(null);
                                          reader.next();
                                            
                                            reader.next();
                                          
                                      }else{
                                    
                                                object.setMultipleSenders(it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent.Factory.parse(reader));
                                              
                                        reader.next();
                                    }
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Note").equals(reader.getName()) || new javax.xml.namespace.QName("","Note").equals(reader.getName()) ){
                                
                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                          object.setNote(null);
                                          reader.next();
                                            
                                            reader.next();
                                          
                                      }else{
                                    
                                                object.setNote(it.opencontent.nttdata._2012.pi3.ArrayOfNote.Factory.parse(reader));
                                              
                                        reader.next();
                                    }
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Object").equals(reader.getName()) || new javax.xml.namespace.QName("","Object").equals(reader.getName()) ){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setObject(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","PersonalDocument").equals(reader.getName()) || new javax.xml.namespace.QName("","PersonalDocument").equals(reader.getName()) ){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"PersonalDocument" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPersonalDocument(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Predisposed").equals(reader.getName()) || new javax.xml.namespace.QName("","Predisposed").equals(reader.getName()) ){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"Predisposed" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPredisposed(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","PrivateDocument").equals(reader.getName()) || new javax.xml.namespace.QName("","PrivateDocument").equals(reader.getName()) ){
                                
                                    nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                    if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                        throw new org.apache.axis2.databinding.ADBException("The element: "+"PrivateDocument" +"  cannot be null");
                                    }
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setPrivateDocument(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToBoolean(content));
                                              
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","ProtocolDate").equals(reader.getName()) || new javax.xml.namespace.QName("","ProtocolDate").equals(reader.getName()) ){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setProtocolDate(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","ProtocolSender").equals(reader.getName()) || new javax.xml.namespace.QName("","ProtocolSender").equals(reader.getName()) ){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setProtocolSender(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Recipients").equals(reader.getName()) || new javax.xml.namespace.QName("","Recipients").equals(reader.getName()) ){
                                
                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                          object.setRecipients(null);
                                          reader.next();
                                            
                                            reader.next();
                                          
                                      }else{
                                    
                                                object.setRecipients(it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent.Factory.parse(reader));
                                              
                                        reader.next();
                                    }
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","RecipientsCC").equals(reader.getName()) || new javax.xml.namespace.QName("","RecipientsCC").equals(reader.getName()) ){
                                
                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                          object.setRecipientsCC(null);
                                          reader.next();
                                            
                                            reader.next();
                                          
                                      }else{
                                    
                                                object.setRecipientsCC(it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent.Factory.parse(reader));
                                              
                                        reader.next();
                                    }
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Register").equals(reader.getName()) || new javax.xml.namespace.QName("","Register").equals(reader.getName()) ){
                                
                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                          object.setRegister(null);
                                          reader.next();
                                            
                                            reader.next();
                                          
                                      }else{
                                    
                                                object.setRegister(it.opencontent.nttdata._2012.pi3.Register.Factory.parse(reader));
                                              
                                        reader.next();
                                    }
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Sender").equals(reader.getName()) || new javax.xml.namespace.QName("","Sender").equals(reader.getName()) ){
                                
                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                          object.setSender(null);
                                          reader.next();
                                            
                                            reader.next();
                                          
                                      }else{
                                    
                                                object.setSender(it.opencontent.nttdata._2012.pi3.Correspondent.Factory.parse(reader));
                                              
                                        reader.next();
                                    }
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Signature").equals(reader.getName()) || new javax.xml.namespace.QName("","Signature").equals(reader.getName()) ){
                                
                                       nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                       if (!"true".equals(nillableValue) && !"1".equals(nillableValue)){
                                    

                                    java.lang.String content = reader.getElementText();
                                    
                                              object.setSignature(
                                                    org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
                                            
                                       } else {
                                           
                                           
                                           reader.getElementText(); // throw away text nodes if any.
                                       }
                                      
                                        reader.next();
                                    
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                
                                    
                                    while (!reader.isStartElement() && !reader.isEndElement()) reader.next();
                                
                                    if (reader.isStartElement() && new javax.xml.namespace.QName("http://nttdata.com/2012/Pi3","Template").equals(reader.getName()) || new javax.xml.namespace.QName("","Template").equals(reader.getName()) ){
                                
                                      nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance","nil");
                                      if ("true".equals(nillableValue) || "1".equals(nillableValue)){
                                          object.setTemplate(null);
                                          reader.next();
                                            
                                            reader.next();
                                          
                                      }else{
                                    
                                                object.setTemplate(it.opencontent.nttdata._2012.pi3.Template.Factory.parse(reader));
                                              
                                        reader.next();
                                    }
                              }  // End of if for expected property start element
                                
                                    else {
                                        
                                    }
                                  
                            while (!reader.isStartElement() && !reader.isEndElement())
                                reader.next();
                            
                                if (reader.isStartElement())
                                // 2 - A start element we are not expecting indicates a trailing invalid property
                                
                                    throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
                                



            } catch (javax.xml.stream.XMLStreamException e) {
                throw new java.lang.Exception(e);
            }

            return object;
        }

        }//end of factory class

        

        }
           
    