

/**
 * Token.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */

    package it.opencontent.nttdata._2012.pi3;

    /*
     *  Token java interface
     */

    public interface Token {
          

        /**
          * Auto generated method signature
          * 
                    * @param getAuthenticationToken0
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetAuthenticationTokenResponse getAuthenticationToken(

                        it.opencontent.nttdata._2012.pi3.GetAuthenticationToken getAuthenticationToken0)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getAuthenticationToken0
            
          */
        public void startgetAuthenticationToken(

            it.opencontent.nttdata._2012.pi3.GetAuthenticationToken getAuthenticationToken0,

            final it.opencontent.nttdata._2012.pi3.TokenCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getToken2
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetTokenResponse getToken(

                        it.opencontent.nttdata._2012.pi3.GetToken getToken2)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getToken2
            
          */
        public void startgetToken(

            it.opencontent.nttdata._2012.pi3.GetToken getToken2,

            final it.opencontent.nttdata._2012.pi3.TokenCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        
       //
       }
    