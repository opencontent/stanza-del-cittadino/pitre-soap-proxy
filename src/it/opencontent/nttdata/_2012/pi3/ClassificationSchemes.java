

/**
 * ClassificationSchemes.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */

    package it.opencontent.nttdata._2012.pi3;

    /*
     *  ClassificationSchemes java interface
     */

    public interface ClassificationSchemes {
          

        /**
          * Auto generated method signature
          * 
                    * @param getClassificationSchemeById0
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetClassificationSchemeByIdResponse getClassificationSchemeById(

                        it.opencontent.nttdata._2012.pi3.GetClassificationSchemeById getClassificationSchemeById0)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getClassificationSchemeById0
            
          */
        public void startgetClassificationSchemeById(

            it.opencontent.nttdata._2012.pi3.GetClassificationSchemeById getClassificationSchemeById0,

            final it.opencontent.nttdata._2012.pi3.ClassificationSchemesCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getActiveClassificationScheme2
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetActiveClassificationSchemeResponse getActiveClassificationScheme(

                        it.opencontent.nttdata._2012.pi3.GetActiveClassificationScheme getActiveClassificationScheme2)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getActiveClassificationScheme2
            
          */
        public void startgetActiveClassificationScheme(

            it.opencontent.nttdata._2012.pi3.GetActiveClassificationScheme getActiveClassificationScheme2,

            final it.opencontent.nttdata._2012.pi3.ClassificationSchemesCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        /**
          * Auto generated method signature
          * 
                    * @param getAllClassificationSchemes4
                
         */

         
                     public it.opencontent.nttdata._2012.pi3.GetAllClassificationSchemesResponse getAllClassificationSchemes(

                        it.opencontent.nttdata._2012.pi3.GetAllClassificationSchemes getAllClassificationSchemes4)
                        throws java.rmi.RemoteException
             ;

        
         /**
            * Auto generated method signature for Asynchronous Invocations
            * 
                * @param getAllClassificationSchemes4
            
          */
        public void startgetAllClassificationSchemes(

            it.opencontent.nttdata._2012.pi3.GetAllClassificationSchemes getAllClassificationSchemes4,

            final it.opencontent.nttdata._2012.pi3.ClassificationSchemesCallbackHandler callback)

            throws java.rmi.RemoteException;

     

        
       //
       }
    