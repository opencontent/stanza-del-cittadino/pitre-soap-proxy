
/**
 * RolesCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.7.3  Built on : May 30, 2016 (04:08:57 BST)
 */

    package it.opencontent.nttdata._2012.pi3;

    /**
     *  RolesCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class RolesCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public RolesCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public RolesCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getUsersInRole method
            * override this method for handling normal response from getUsersInRole operation
            */
           public void receiveResultgetUsersInRole(
                    it.opencontent.nttdata._2012.pi3.GetUsersInRoleResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getUsersInRole operation
           */
            public void receiveErrorgetUsersInRole(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getRoles method
            * override this method for handling normal response from getRoles operation
            */
           public void receiveResultgetRoles(
                    it.opencontent.nttdata._2012.pi3.GetRolesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getRoles operation
           */
            public void receiveErrorgetRoles(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getRolesForEnabledActions method
            * override this method for handling normal response from getRolesForEnabledActions operation
            */
           public void receiveResultgetRolesForEnabledActions(
                    it.opencontent.nttdata._2012.pi3.GetRolesForEnabledActionsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getRolesForEnabledActions operation
           */
            public void receiveErrorgetRolesForEnabledActions(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getRole method
            * override this method for handling normal response from getRole operation
            */
           public void receiveResultgetRole(
                    it.opencontent.nttdata._2012.pi3.GetRoleResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getRole operation
           */
            public void receiveErrorgetRole(java.lang.Exception e) {
            }
                


    }
    