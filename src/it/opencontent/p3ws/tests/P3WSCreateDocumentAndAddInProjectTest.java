package it.opencontent.p3ws.tests;

import it.opencontent.p3ws.client.P3WSClient;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Base64;

import org.codehaus.jettison.json.JSONObject;


public class P3WSCreateDocumentAndAddInProjectTest {

  //------------------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------------------
  //solo per test
  public static void main(String[] args) {

    JSONObject jObjd = new JSONObject();

    try {


      System.out.println("---------------------------------------------------------------------------------------");
      System.out.println("Instazio il client");
      P3WSClient p3WSClient = new P3WSClient("rataa-test");

      System.out.println("Preparo parametri");


      // Test vallelaghi
      String codeNodeClassification = "1";
      String[] recipientIDArray = new String[]{"16275400"};
      String[] trasmissionIDArray = new String[]{"621coll"};
      String documnetType = "A";
      
      // Test C
      /*String codeNodeClassification = "6.3";
      String[] recipientIDArray = new String[]{"79935296"};
      String[] trasmissionIDArray = new String[]{"A916PROT"};
      String documnetType = "A";*/


      //per prod
	  /*String codeNodeClassification = "6.3";
	  String[] recipientIDArray = new String[]{"C393RPROT"};
	  String[] trasmissionIDArray = new String[]{"C393RPROT"};*/


      String projectDescription = "Stanza del Cittadino TEST protocollo";
      //String codeAdm = "c_m361";
      //String filePath = "C:\\Projects\\P3Wrapper\\src\\it\\opencontent\\p3ws\\tests\\file\\test.p7m";
      String fileName = "Test.pdf";
      String filePath = "/Applications/MAMP/htdocs/PITreWrapper/src/it/opencontent/p3ws/tests/file/test.pdf";
      String documentDescription = "Stanza del Cittadino TEST protocollo -- descrizione";
      String documentObj = "Stanza del Cittadino TEST protocollo -- oggetto";
      String[] recipientTypeIDArray = new String[]{"R"};

      File fileReal = new File(filePath);
      byte fileContent[] = new byte[(int) fileReal.length()];

      FileInputStream fin = new FileInputStream(fileReal);
      int read = 0;
      ByteArrayOutputStream ous = new ByteArrayOutputStream();
      while ((read = fin.read(fileContent)) != -1) {
        ous.write(fileContent, 0, read);
      }
      fin.close();
      String file = Base64.getEncoder().encodeToString(fileContent);

      //per creare un nuovo project
      boolean createProject = true;
      String idProject = null;

      //per creare il docuemnto in un project già esistente
      //boolean createProject = false;
      //String idProject = "1295730";

      String senderName = "Mario";
      String senderSurname = "Rossi";
      String senderCf = "RSSMRA80A01H501U";
      String senderEmail = "mario.rossi@email.com";

      System.out.println("Eseguo la chiamata al WS (createDocumentAndAddInProject)");
      jObjd = p3WSClient.createDocumentAndAddInProject(fileName, file, codeNodeClassification, projectDescription, documentDescription, documentObj, recipientIDArray, recipientTypeIDArray, trasmissionIDArray, createProject, idProject, documnetType, senderName, senderSurname, senderCf, senderEmail);

      System.out.println("Stampo il risultato");
      System.out.println(jObjd.toString());
      System.out.println("---------------------------------------------------------------------------------------");


    } catch (Exception e) {
      e.printStackTrace();
    }

  }
}
