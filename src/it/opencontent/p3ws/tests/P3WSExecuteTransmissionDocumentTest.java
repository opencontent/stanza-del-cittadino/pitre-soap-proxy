package it.opencontent.p3ws.tests;

import it.opencontent.nttdata._2012.pi3.Document;
import it.opencontent.p3ws.client.P3WSClient;

import org.codehaus.jettison.json.JSONObject;


public class P3WSExecuteTransmissionDocumentTest {

    //------------------------------------------------------------------------------------------------------------------
    //
    //------------------------------------------------------------------------------------------------------------------
	 //solo per test
	 public static void main(String []args){

    	JSONObject jObjd=new JSONObject();
    	
		try{
			
			System.out.println("Instazio il client");
			P3WSClient p3WSClient = new P3WSClient(null);

			System.out.println("Eseguo la chiamata al WS");
			
			String[] recipientIDArray = new String[]{"c_m361CPROTSEGR"};
			
			//document
			Document document = new Document();
			document.setId("152453216");
		
			p3WSClient.executeTransmissionDocument(recipientIDArray, document);

			System.out.println("Stampo il risultato");
			System.out.println(jObjd.toString());

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 }
}
