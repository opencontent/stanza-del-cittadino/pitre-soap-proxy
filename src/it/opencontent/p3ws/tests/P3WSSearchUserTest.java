package it.opencontent.p3ws.tests;

import it.opencontent.nttdata._2012.pi3.Correspondent;
import it.opencontent.p3ws.client.P3WSClient;
import it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent;
import org.codehaus.jettison.json.JSONObject;


public class P3WSSearchUserTest {

    //------------------------------------------------------------------------------------------------------------------
    //
    //------------------------------------------------------------------------------------------------------------------
	 //solo per test
	 public static void main(String []args){

    	//JSONObject jObjd=new JSONObject();
    	
		try{

      String code = "UTENTE_STANZACITTADINO2";
			System.out.println("Instazio il client");
      P3WSClient p3WSClient = new P3WSClient("apss_2-prod");

			System.out.println("Eseguo la chiamata al WS");
      ArrayOfCorrespondent result = p3WSClient.searchUser(code);

			System.out.println("Stampo il risultato");

      Correspondent[] receivers = result.getCorrespondent();

      for (int i = 0; i < receivers.length; i++) {
        System.out.println(receivers[i].getId());
        System.out.println(receivers[i].getCode());
        System.out.println(receivers[i].getAOOCode());
      }

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 }
}
