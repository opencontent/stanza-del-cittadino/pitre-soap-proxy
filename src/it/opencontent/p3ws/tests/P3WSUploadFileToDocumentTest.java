package it.opencontent.p3ws.tests;

import it.opencontent.p3ws.client.P3WSClient;

import org.codehaus.jettison.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

import java.util.Base64;


public class P3WSUploadFileToDocumentTest {

  //------------------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------------------
  //solo per test
  public static void main(String[] args) {

    JSONObject jObjd = new JSONObject();

    try {


      System.out.println("---------------------------------------------------------------------------------------");
      System.out.println("Instazio il client");
      P3WSClient p3WSClient = new P3WSClient("cavedine-test");

      System.out.println("Preparo parametri");

      //String codeAdm = "CCT_CAL";
      String documentId = "79875409";
      //String filePath="C:\\Projects\\P3Wrapper\\src\\it\\opencontent\\p3ws\\tests\\file\\test.p7m";
      //String filePath = "/Applications/MAMP/htdocs/PITreWrapper/src/it/opencontent/p3ws/tests/file/test.p7m";
      String fileName = "Test-UploadFileToDocumentTest.pdf.p7m";
      String filePath = "/Applications/MAMP/htdocs/PITreWrapper/src/it/opencontent/p3ws/tests/file/test.p7m";

      File fileReal = new File(filePath);
      byte fileContent[] = new byte[(int) fileReal.length()];

      FileInputStream fin = new FileInputStream(fileReal);
      int read = 0;
      ByteArrayOutputStream ous = new ByteArrayOutputStream();
      while ((read = fin.read(fileContent)) != -1) {
        ous.write(fileContent, 0, read);
      }
      fin.close();
      String file = Base64.getEncoder().encodeToString(fileContent);

      String attachmentDescription = "Descrizione di test dell'allegato 1";
      boolean createAttachment = true;
      String attachmentType = "U";

      System.out.println("Eseguo la chiamata al WS (uploadFileToDocument)");
      jObjd = p3WSClient.uploadFileToDocument(documentId, fileName, file, attachmentDescription, createAttachment, attachmentType);

      System.out.println("Stampo il risultato");
      System.out.println(jObjd.toString());
      System.out.println("---------------------------------------------------------------------------------------");


    } catch (Exception e) {
      e.printStackTrace();
    }

  }
}
