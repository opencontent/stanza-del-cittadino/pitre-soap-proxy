package it.opencontent.p3ws.tests;

import it.opencontent.p3ws.client.P3WSClient;

import org.codehaus.jettison.json.JSONObject;


public class P3WSGetDocumentTest {

    //------------------------------------------------------------------------------------------------------------------
    //
    //------------------------------------------------------------------------------------------------------------------
	 //solo per test
	 public static void main(String []args){

    	JSONObject jObjd=new JSONObject();
    	
		try{
			
			System.out.println("Instazio il client");
			P3WSClient p3WSClient = new P3WSClient("vallelaghi-test");

			// {"data":{"id_doc":"80309455","id_file":"80309455","id_proj":"80309453"}
			System.out.println("Eseguo la chiamata al WS");
			jObjd = p3WSClient.getDocument("80309455", false);

			System.out.println("Stampo il risultato");
			System.out.println(jObjd.toString());

		}catch(Exception e){
			e.printStackTrace();
		}
		
	 }
}
