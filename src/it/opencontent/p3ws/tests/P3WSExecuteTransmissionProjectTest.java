package it.opencontent.p3ws.tests;

import it.opencontent.nttdata._2012.pi3.Document;
import it.opencontent.p3ws.client.P3WSClient;

import org.codehaus.jettison.json.JSONObject;


public class P3WSExecuteTransmissionProjectTest {

    //------------------------------------------------------------------------------------------------------------------
    //
    //------------------------------------------------------------------------------------------------------------------
	 //solo per test
	 public static void main(String []args){

    	JSONObject jObjd=new JSONObject();
    	
		try{
			
			System.out.println("---------------------------------------------------------------------------------------");
	        System.out.println("Instazio il client");
		    P3WSClient p3WSClient = new P3WSClient("vallelaghi-test");

		    System.out.println("Preparo parametri");

			System.out.println("Eseguo la chiamata al WS");
			
			//String[] trasmissionIDArray = new String[]{"c_m362PROT"};
			String[] trasmissionIDArray = new String[]{"c_m362CUEPR"};
			
			p3WSClient.executeTransmissionProject(trasmissionIDArray, "80309453");
			
			p3WSClient.getProjectByDocument("80309455");


		}catch(Exception e){
			e.printStackTrace();
		}
		
	 }
}
