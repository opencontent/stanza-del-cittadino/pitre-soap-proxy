package it.opencontent.p3ws.tests;

import it.opencontent.nttdata._2012.pi3.Document;
import it.opencontent.p3ws.client.P3WSClient;

import org.codehaus.jettison.json.JSONObject;

public class P3WSProtocolPredisposedTest {

	// ------------------------------------------------------------------------------------------------------------------
	//
	// ------------------------------------------------------------------------------------------------------------------
	// solo per test
	public static void main(String[] args) {

		JSONObject jObjd = new JSONObject();
		JSONObject jObjdPredisposed = new JSONObject();

		try {

			System.out.println("----------------------------------------------------------------------------------");
			System.out.println("Instazio il client");
			P3WSClient p3WSClient = new P3WSClient("mori_d-prod");


			// Qui inseriamo l'id del documento di cui fare il predisposto
			String documentId = "598080329";
			String[] trasmissionIDArray = new String[]{"c_f728PROT","c_f728RSEAT","c_f728CSEAT","c_f728SSIND"};
			
			jObjd = p3WSClient.getDocument(documentId, false);
			
			System.out.println("Stampo il risultato");
			System.out.println(jObjd.toString());
			
			jObjdPredisposed = p3WSClient.protocolPredisposed(documentId, trasmissionIDArray);
			System.out.println("Stampo il risultato: protocolPredisposed");
			System.out.println(jObjdPredisposed.toString());
			

			/*if (!documentId.isEmpty()) {

				String attachmentDescription = "Descrizione di test dell'allegato 1";
				boolean createAttachment = true;
				String attachmentType = "U";
				System.out.println("Eseguo la chiamata al WS (uploadFileToDocument)");
				jObjd = p3WSClient.uploadFileToDocument(documentId, fileName, file, attachmentDescription,
						createAttachment, attachmentType);
				System.out.println("Stampo il risultato: uploadFileToDocument");
				System.out.println(jObjd.toString());

				jObjd = p3WSClient.protocolPredisposed(documentId, trasmissionIDArray);
				System.out.println("Stampo il risultato: protocolPredisposed");
				System.out.println(jObjd.toString());
			}*/

			// jObjd = p3WSClient.getDocument("80310167", false);

			System.out.println("---------------------------------------------------------------------------------");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
