package it.opencontent.p3ws.tests;

import it.opencontent.p3ws.client.P3WSClient;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Base64;

import org.codehaus.jettison.json.JSONObject;


public class P3WSPredisposedTest {

  //------------------------------------------------------------------------------------------------------------------
  //
  //------------------------------------------------------------------------------------------------------------------
  //solo per test
  public static void main(String[] args) {

    JSONObject jObjd = new JSONObject();

    try {


      System.out.println("---------------------------------------------------------------------------------------");
      System.out.println("Instazio il client");
      P3WSClient p3WSClient = new P3WSClient("vallelaghi-test");
      //P3WSClient p3WSClient = new P3WSClient("ledro_sdc-test");

      System.out.println("Preparo parametri");


      // Test vallelaghi
      String codeNodeClassification = "1";
      String[] recipientIDArray = new String[]{"79901642"};
      String[] trasmissionIDArray = new String[]{"c_m362PROT"};
      String documnetType = "P";
      
      // Test regione
      /*String codeNodeClassification = "7.12";
      String[] recipientIDArray = new String[]{"USR_SDC_c_m313"};
      String[] trasmissionIDArray = new String[]{"c_m313CSF"};
      String documnetType = "A";*/
      


      String projectDescription = "Stanza del Cittadino TEST protocollo";
      //String codeAdm = "c_m361";
      //String filePath = "C:\\Projects\\P3Wrapper\\src\\it\\opencontent\\p3ws\\tests\\file\\test.p7m";
      String fileName = "Test.pdf";
      String filePath = "/Applications/MAMP/htdocs/pitre-soap-proxy/src/it/opencontent/p3ws/tests/file/test.pdf";
      String documentDescription = "Stanza del Cittadino TEST protocollo -- descrizione";
      String documentObj = "Stanza del Cittadino TEST protocollo -- oggetto";
      String[] recipientTypeIDArray = new String[]{"R"};

      File fileReal = new File(filePath);
      byte fileContent[] = new byte[(int) fileReal.length()];

      FileInputStream fin = new FileInputStream(fileReal);
      int read = 0;
      ByteArrayOutputStream ous = new ByteArrayOutputStream();
      while ((read = fin.read(fileContent)) != -1) {
        ous.write(fileContent, 0, read);
      }
      fin.close();
      String file = Base64.getEncoder().encodeToString(fileContent);

      //per creare un nuovo project
      //{"data":{"id_doc":"80309455","id_file":"80309455","id_proj":"80309453"},"status":"success","message":"Elaborazione eseguita correttamente"}
      /*boolean createProject = false;
      String idProject = "80309453";*/
      boolean createProject = true;
      String idProject = null;

      String senderName = "Mario";
      String senderSurname = "Rossi";
      String senderCf = "RSSMRA80A01H501U";
      String senderEmail = "mario.rossi@email.com";

      System.out.println("Eseguo la chiamata al WS (createDocumentPredisposed)");
      
      
      jObjd = p3WSClient.createDocumentPredisposed(fileName, file, codeNodeClassification, projectDescription, documentDescription, documentObj, recipientIDArray, recipientTypeIDArray, trasmissionIDArray, createProject, idProject, documnetType, senderName, senderSurname, senderCf, senderEmail);
      System.out.println("Stampo il risultato: createDocumentPredisposed");
      System.out.println(jObjd.toString());
      
      
      String documentId = jObjd.getJSONObject("data").getString("id_doc");
      System.out.println("Stampo l'id del documento: " + documentId);
      
      if (!documentId.isEmpty()) {
    	  
    	  String attachmentDescription = "Descrizione di test dell'allegato 1";
          boolean createAttachment = true;
          String attachmentType = "U";
    	  System.out.println("Eseguo la chiamata al WS (uploadFileToDocument)");
          jObjd = p3WSClient.uploadFileToDocument(documentId, fileName, file, attachmentDescription, createAttachment, attachmentType);
          System.out.println("Stampo il risultato: uploadFileToDocument");
          System.out.println(jObjd.toString());
    	      	 
          jObjd = p3WSClient.protocolPredisposed(documentId, trasmissionIDArray);
          System.out.println("Stampo il risultato: protocolPredisposed");
          System.out.println(jObjd.toString());
      }     
     
      //jObjd = p3WSClient.getDocument("80310167", false);
      
      System.out.println("---------------------------------------------------------------------------------------");


    } catch (Exception e) {
      e.printStackTrace();
    }

  }
}
