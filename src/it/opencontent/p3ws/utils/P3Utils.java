package it.opencontent.p3ws.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

public class P3Utils {
	

	private static String propertiesDIR = "properties/";
	
	public static String LEVEL_SUCCESS = "success";
	public static String LEVEL_WARNING = "warning";
	public static String LEVEL_ERROR = "error";

	public static String DEFAULT_SUCCESS_MESSAGE = "Elaborazione eseguita correttamente";
	
	//return last collection element
	public static Object getLastElement(final Collection c) {
	    final Iterator itr = c.iterator();
	    Object lastElement = itr.next();
	    while(itr.hasNext()) {
	        lastElement=itr.next();
	    }
	    return lastElement;
	}

	//return values array of properties from specifica properties file
	public static Map<String, String> getProperties(String instance) {
		
		Map<String, String> properties = new HashMap<>();
		Properties prop = new Properties();
		InputStream input = null;

		try {

			input = new FileInputStream(propertiesDIR+instance+"-config.properties");

			// load a properties file
			prop.load(input);

			properties.put("wsURL", prop.getProperty("wsURL").trim());
			properties.put("keyStore", prop.getProperty("keyStore").trim());
			properties.put("trustStore", prop.getProperty("trustStore").trim());
			properties.put("keyStorePassword", prop.getProperty("keyStorePassword").trim());
			properties.put("codeAdm", prop.getProperty("codeAdm").trim());
			properties.put("codeRoleLogin", prop.getProperty("codeRoleLogin").trim());
			properties.put("codeRegister", prop.getProperty("codeRegister").trim());
			properties.put("userName", prop.getProperty("userName").trim());
			properties.put("userID", prop.getProperty("userID").trim());
			
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		return properties;

	}
	
	//gestione custom mime type per p7m (FIXME: da affinare)
	public static String getCustomMimeType(String fileName) {

		String contentType = MimeTypeResolver.findMimeType(fileName);
		if("application/pkcs7-mime".equals(contentType)){
			int index = fileName.indexOf(".");
			String nameNoExt = fileName.substring(0, index);
			fileName = nameNoExt+".pdf.p7m";

		}
		return fileName;
	}
	
}
