package it.opencontent.p3ws.jetty;

import org.eclipse.jetty.server.Server;

/**
 * The simplest possible Jetty server.
 */
public class JettyServer
{
    public static void main( String[] args ) throws Exception
    {
        Server server = new Server(8080);
        
        server.setAttribute("org.eclipse.jetty.server.Request.maxFormContentSize", 52428800);
        
        //qui vanno aggiunti tutti gli handler
        server.setHandler(new JettyMainHandler());
        
        server.start();
        server.dumpStdErr();
        server.join();
    }
}