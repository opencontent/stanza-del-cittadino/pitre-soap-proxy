package it.opencontent.p3ws.jetty;

import it.opencontent.p3ws.client.P3WSClient;
import it.opencontent.p3ws.utils.P3Utils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;

import org.codehaus.jettison.json.JSONObject;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import java.security.MessageDigest;

public class JettyMainHandler extends AbstractHandler {

	public JettyMainHandler() {

	}


	@Override
	public void handle(String target,
			Request baseRequest,
			HttpServletRequest request,
			HttpServletResponse response) throws IOException,
	ServletException {
		try {

			//response.setContentType("text/html; charset=utf-8");
			response.setContentType("application/json; charset=utf-8");

			response.setStatus(HttpServletResponse.SC_OK);

			String method = request.getParameter("method");
			String instance = request.getParameter("instance");

			JSONObject jObjd = new JSONObject();

			//instance è obbligatorio sempre
			if (instance == null || "".equals(instance)) {
				JSONObject jObInner = new JSONObject();
				jObjd.put("data", jObInner);
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "instance è un valore obbligatorio");

				printResult(baseRequest, response, jObjd);
				return;
			}

			// method è obbligatorio sempre
			if (method == null || "".equals(method)) {
				JSONObject jObInner = new JSONObject();
				jObjd.put("data", jObInner);
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "method è un valore obbligatorio");

				printResult(baseRequest, response, jObjd);
				return;
			}

			P3WSClient p3WSClient = new P3WSClient(instance);

			//getDocument
			// ------------------------------------------------------------------------------
			if ("getDocument".equalsIgnoreCase(method)) {

				String documentId = request.getParameter("documentId"); //test 1290126
				String getFile = request.getParameter("getFile");

				boolean getFileBool = false;

				if ("true".equals(getFile) || "1".equals(getFile)) {
					getFileBool = true;
				}

				jObjd = p3WSClient.getDocument(documentId, getFileBool);

				//protocolPredisposed
				// ------------------------------------------------------------------------------
			} else if ("protocolPredisposed".equalsIgnoreCase(method)) {

				String documentId = request.getParameter("documentId"); //test 1290126
				String transmission = request.getParameter("trasmissionIDArray");
				String[] trasmissionIDArray = transmission.split(",");

				jObjd = p3WSClient.protocolPredisposed(documentId, trasmissionIDArray);
				
				
			} else if ("createDocumentPredisposed".equalsIgnoreCase(method)) {

				String fileName = request.getParameter("fileName");
				String file = request.getParameter("file");
				String checksum = request.getParameter("checksum");
				String codeNodeClassification = request.getParameter("codeNodeClassification");
				String projectDescription = request.getParameter("projectDescription");
				String documentDescription = request.getParameter("documentDescription");
				String documentObj = request.getParameter("documentObj");
				String[] recipientIDArray = request.getParameterValues("recipientIDArray");
				String[] recipientTypeIDArray = request.getParameterValues("recipientTypeIDArray");
				//String[] trasmissionIDArray = request.getParameterValues("trasmissionIDArray");
				String transmission = request.getParameter("trasmissionIDArray");
				String[] trasmissionIDArray = transmission.split(",");
				String createProject = request.getParameter("createProject");
				String idProject = request.getParameter("idProject");
				String documentType = request.getParameter("documentType");
				String senderName = request.getParameter("senderName");
				String senderSurname = request.getParameter("senderSurname");
				String senderCf = request.getParameter("senderCf");
				String senderEmail = request.getParameter("senderEmail");

				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(file.getBytes());
				byte[] digest = md.digest();
				String fileHash = DatatypeConverter.printHexBinary(digest).toUpperCase();

				if (!checksum.equals(fileHash)) {
					JSONObject jObInner = new JSONObject();
					jObjd.put("data", jObInner);
					jObjd.put("status", P3Utils.LEVEL_ERROR);
					jObjd.put("message", "Il file ricevuto non corrisponde con il checksum passato");
					printResult(baseRequest, response, jObjd);
					return;
				}


				boolean createProjectBool = false;

				if ("true".equals(createProject) || "1".equals(createProject)) {
					createProjectBool = true;
				}

				jObjd = p3WSClient.createDocumentPredisposed(
						fileName,
						file,
						codeNodeClassification,
						projectDescription,
						documentDescription,
						documentObj,
						recipientIDArray,
						recipientTypeIDArray,
						trasmissionIDArray,
						createProjectBool,
						idProject,
						documentType,
						senderName,
						senderSurname,
						senderCf,
						senderEmail
						);

				//createDocumentAndAddInProject
				// ------------------------------------------------------------------------------
			} else if ("createDocumentAndAddInProject".equalsIgnoreCase(method)) {

				String fileName = request.getParameter("fileName");
				String file = request.getParameter("file");
				String checksum = request.getParameter("checksum");
				String codeNodeClassification = request.getParameter("codeNodeClassification");
				String projectDescription = request.getParameter("projectDescription");
				String documentDescription = request.getParameter("documentDescription");
				String documentObj = request.getParameter("documentObj");
				String[] recipientIDArray = request.getParameterValues("recipientIDArray");
				String[] recipientTypeIDArray = request.getParameterValues("recipientTypeIDArray");
				//String[] trasmissionIDArray = request.getParameterValues("trasmissionIDArray");
				String transmission = request.getParameter("trasmissionIDArray");
				String[] trasmissionIDArray = transmission.split(",");
				String createProject = request.getParameter("createProject");
				String idProject = request.getParameter("idProject");
				String documentType = request.getParameter("documentType");
				String senderName = request.getParameter("senderName");
				String senderSurname = request.getParameter("senderSurname");
				String senderCf = request.getParameter("senderCf");
				String senderEmail = request.getParameter("senderEmail");

				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(file.getBytes());
				byte[] digest = md.digest();
				String fileHash = DatatypeConverter.printHexBinary(digest).toUpperCase();

				if (!checksum.equals(fileHash)) {
					JSONObject jObInner = new JSONObject();
					jObjd.put("data", jObInner);
					jObjd.put("status", P3Utils.LEVEL_ERROR);
					jObjd.put("message", "Il file ricevuto non corrisponde con il checksum passato");
					printResult(baseRequest, response, jObjd);
					return;
				}


				boolean createProjectBool = false;

				if ("true".equals(createProject) || "1".equals(createProject)) {
					createProjectBool = true;
				}

				jObjd = p3WSClient.createDocumentAndAddInProject(
						fileName,
						file,
						codeNodeClassification,
						projectDescription,
						documentDescription,
						documentObj,
						recipientIDArray,
						recipientTypeIDArray,
						trasmissionIDArray,
						createProjectBool,
						idProject,
						documentType,
						senderName,
						senderSurname,
						senderCf,
						senderEmail
						);


				//uploadFileToDocument
				// ------------------------------------------------------------------------------
			} else if ("uploadFileToDocument".equalsIgnoreCase(method)) {

				String documentId = request.getParameter("documentId");
				String fileName = request.getParameter("fileName");
				String file = request.getParameter("file");
				String checksum = request.getParameter("checksum");
				String attachmentDescription = request.getParameter("attachmentDescription");

				String createAttachment = request.getParameter("createAttachment");
				String attachmentType = request.getParameter("attachmentType");

				MessageDigest md = MessageDigest.getInstance("MD5");
				md.update(file.getBytes());
				byte[] digest = md.digest();
				String fileHash = DatatypeConverter.printHexBinary(digest).toUpperCase();

				if (!checksum.equals(fileHash)) {
					JSONObject jObInner = new JSONObject();
					jObjd.put("data", jObInner);
					jObjd.put("status", P3Utils.LEVEL_ERROR);
					jObjd.put("message", "Il file ricevuto non corrisponde con il checksum passato");
					printResult(baseRequest, response, jObjd);
					return;
				}

				boolean attachmentTypeBool = true;

				if ("false".equals(attachmentType) || "0".equals(attachmentType)) {
					attachmentTypeBool = false;
				}

				jObjd = p3WSClient.uploadFileToDocument(documentId, fileName, file, attachmentDescription, attachmentTypeBool, createAttachment);

			}

			printResult(baseRequest, response, jObjd);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void printResult(Request baseRequest, HttpServletResponse response, JSONObject jObjd) {

		try {

			PrintWriter out = response.getWriter();
			out.println(jObjd.toString());
			baseRequest.setHandled(true);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
