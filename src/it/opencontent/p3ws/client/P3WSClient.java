package it.opencontent.p3ws.client;

import it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_addressbook_searchcorrespondents.SearchCorrespondentsRequest;
import it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_classificationscheme_getactiveclassificationscheme.GetActiveClassificationSchemeRequest;
import it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_documents_adddocinproject.AddDocInProjectRequest;
import it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_documents_createdocument.CreateDocumentRequest;
import it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_documents_createdocumentandaddinproject.CreateDocumentAndAddInProjectRequest;
import it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_documents_editdocument.EditDocumentRequest;
import it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_documents_getdocument.GetDocumentRequest;
import it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_documents_uploadfiletodocument.UploadFileToDocumentRequest;
import it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_projects_createproject.CreateProjectRequest;
import it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_projects_getproject.GetProjectRequest;
import it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_projects_getprojectsbydocument.GetProjectsByDocumentRequest;
import it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_token_getauthenticationtoken.GetAuthenticationTokenRequest;
import it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_transmissions_executetransmissiondocument.ExecuteTransmissionDocumentRequest;
import it.opencontent.datacontract.schemas._2004._07.vtdocsws_services_transmissions_executetransmissionproject.ExecuteTransmissionProjectRequest;
import it.opencontent.nttdata._2012.pi3.AddDocInProject;
import it.opencontent.nttdata._2012.pi3.AddDocInProjectResponse;
import it.opencontent.nttdata._2012.pi3.AddressBookStub;
import it.opencontent.nttdata._2012.pi3.ArrayOfCorrespondent;
import it.opencontent.nttdata._2012.pi3.ArrayOfFilter;
import it.opencontent.nttdata._2012.pi3.ArrayOfNote;
import it.opencontent.nttdata._2012.pi3.ArrayOfProject;
import it.opencontent.nttdata._2012.pi3.ClassificationScheme;
import it.opencontent.nttdata._2012.pi3.ClassificationSchemesStub;
import it.opencontent.nttdata._2012.pi3.Correspondent;
import it.opencontent.nttdata._2012.pi3.CreateDocument;
import it.opencontent.nttdata._2012.pi3.CreateDocumentAndAddInProject;
import it.opencontent.nttdata._2012.pi3.CreateDocumentAndAddInProjectResponse;
import it.opencontent.nttdata._2012.pi3.CreateDocumentResponse;
import it.opencontent.nttdata._2012.pi3.CreateProject;
import it.opencontent.nttdata._2012.pi3.CreateProjectResponse;
import it.opencontent.nttdata._2012.pi3.Document;
import it.opencontent.nttdata._2012.pi3.DocumentsStub;
import it.opencontent.nttdata._2012.pi3.EditDocument;
import it.opencontent.nttdata._2012.pi3.EditDocumentResponse;
import it.opencontent.nttdata._2012.pi3.ExecuteTransmissionDocument;
import it.opencontent.nttdata._2012.pi3.ExecuteTransmissionDocumentResponse;
import it.opencontent.nttdata._2012.pi3.ExecuteTransmissionProject;
import it.opencontent.nttdata._2012.pi3.ExecuteTransmissionProjectResponse;
import it.opencontent.nttdata._2012.pi3.Filter;
import it.opencontent.nttdata._2012.pi3.FilterType;
import it.opencontent.nttdata._2012.pi3.GetActiveClassificationScheme;
import it.opencontent.nttdata._2012.pi3.GetActiveClassificationSchemeResponse;
import it.opencontent.nttdata._2012.pi3.GetAuthenticationToken;
import it.opencontent.nttdata._2012.pi3.GetAuthenticationTokenResponse;
import it.opencontent.nttdata._2012.pi3.GetDocument;
import it.opencontent.nttdata._2012.pi3.GetDocumentResponse;
import it.opencontent.nttdata._2012.pi3.GetProject;
import it.opencontent.nttdata._2012.pi3.GetProjectResponse;
import it.opencontent.nttdata._2012.pi3.GetProjectsByDocument;
import it.opencontent.nttdata._2012.pi3.GetProjectsByDocumentResponse;
import it.opencontent.nttdata._2012.pi3.Note;
import it.opencontent.nttdata._2012.pi3.Project;
import it.opencontent.nttdata._2012.pi3.ProjectsStub;
import it.opencontent.nttdata._2012.pi3.SearchCorrespondents;
import it.opencontent.nttdata._2012.pi3.SearchCorrespondentsResponse;
import it.opencontent.nttdata._2012.pi3.TokenStub;
import it.opencontent.nttdata._2012.pi3.TransmissionsStub;
import it.opencontent.nttdata._2012.pi3.UploadFileToDocument;
import it.opencontent.p3ws.utils.MimeTypeResolver;
import it.opencontent.p3ws.utils.P3Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Base64;

import javax.activation.DataHandler;
import javax.activation.MimetypesFileTypeMap;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.apache.axiom.soap.SOAP11Constants;
import org.apache.axis2.addressing.AddressingConstants;
import org.apache.axis2.client.Stub;
//import org.apache.commons.codec.binary.Base64;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class P3WSClient {

	private String certificateDIR = "certificate/";

	private String wsURL = "";
	private String keyStore = "";
	private String trustStore = "";
	private String keyStorePassword = "";
	private String codeAdm = "";
	private String codeRoleLogin = "";
	private String codeRegister = "";
	private String userName = "";
	private String userID = "";

	private Correspondent sender = null;

	private GetAuthenticationTokenResponse getAuthenticationTokenResponse = null;
	private String getAuthenticationTokenString = null;

	private Document document = null;
	private Project project = null;

	public P3WSClient(String instance) {

		Map<String, String> properties = new HashMap<>();
		properties = P3Utils.getProperties(instance);

		wsURL = properties.get("wsURL");
		keyStore = properties.get("keyStore");
		trustStore = properties.get("trustStore");
		keyStorePassword = properties.get("keyStorePassword");
		codeAdm = properties.get("codeAdm");
		codeRoleLogin = properties.get("codeRoleLogin");
		codeRegister = properties.get("codeRegister");
		userName = properties.get("userName");
		userID = properties.get("userID");

	}

	// ------------------------------------------------------------------------------------------------------------------
	// getAuthenticationToken
	// ------------------------------------------------------------------------------------------------------------------
	public void getAuthenticationToken() throws Exception {

		if (getAuthenticationTokenResponse == null) {

			System.out.println("ENTER getAuthenticationToken()");
			System.out.println(certificateDIR);
			System.out.println(keyStore);
			System.out.println(trustStore);

			System.out.println(codeAdm);

			try {

				getAuthenticationTokenResponse = new GetAuthenticationTokenResponse();

				TokenStub wws = new TokenStub(wsURL);
				setServiceClientOptions(wws);

				InputStream keystoreInput = Thread.currentThread().getContextClassLoader()
						.getResourceAsStream(certificateDIR + keyStore);
				InputStream truststoreInput = Thread.currentThread().getContextClassLoader()
						.getResourceAsStream(certificateDIR + trustStore);

				// Get keyStore
				KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());

				// if your store is password protected then declare it (it can be null however)
				char[] keyPassword = keyStorePassword.toCharArray();

				// load the stream to your store
				keyStore.load(keystoreInput, keyPassword);

				// initialize a trust manager factory with the trusted store
				KeyManagerFactory keyFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
				keyFactory.init(keyStore, keyPassword);

				// get the trust managers from the factory
				KeyManager[] keyManagers = keyFactory.getKeyManagers();

				// Now get trustStore
				KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());

				// if your store is password protected then declare it (it can be null however)
				// char[] trustPassword = password.toCharArray();

				// load the stream to your store
				trustStore.load(truststoreInput, null);

				// initialize a trust manager factory with the trusted store
				TrustManagerFactory trustFactory = TrustManagerFactory
						.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				trustFactory.init(trustStore);

				// get the trust managers from the factory
				TrustManager[] trustManagers = trustFactory.getTrustManagers();

				// initialize an ssl context to use these managers and set as default
				SSLContext sslContext = SSLContext.getInstance("SSL");
				sslContext.init(keyManagers, trustManagers, null);
				SSLContext.setDefault(sslContext);

				/*
				 * System.setProperty("javax.net.ssl.trustStore",System.getProperty("user.dir")+
				 * certificateDIR+trustStore);
				 * System.setProperty("javax.net.ssl.trustStorePassword", trustStorePassword);
				 * System.setProperty("javax.net.ssl.keyStore",
				 * System.getProperty("user.dir")+certificateDIR+keyStore);
				 * System.setProperty("javax.net.ssl.keyStorePassword", keyStorePassword);
				 */

				GetAuthenticationTokenRequest getAuthenticationTokenRequest = new GetAuthenticationTokenRequest();
				getAuthenticationTokenRequest.setCodeAdm(codeAdm);
				getAuthenticationTokenRequest.setUserName(userName);

        // **DEBUG**
        // System.out.println("codeAdm = "+codeAdm);
				// System.out.println("userName = "+userName);

				GetAuthenticationToken getAuthenticationToken = new GetAuthenticationToken();
				getAuthenticationToken.setRequest(getAuthenticationTokenRequest);

				getAuthenticationTokenResponse = wws.getAuthenticationToken(getAuthenticationToken);
				getAuthenticationTokenString = getAuthenticationTokenResponse.getGetAuthenticationTokenResult()
						.getAuthenticationToken();

				keystoreInput.close();
				truststoreInput.close();

				System.out.println("AuthenticationToken " + getAuthenticationTokenString);

			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			}

			System.out.println("EXIT getAuthenticationToken()");
		}

	}

	// ------------------------------------------------------------------------------------------------------------------
	// generateStub
	// ------------------------------------------------------------------------------------------------------------------
	private void setServiceClientOptions(Stub genericStub) {

		genericStub._getServiceClient().getOptions().setTimeOutInMilliSeconds(6 * 60 * 1000);
		genericStub._getServiceClient().getOptions().setProperty(AddressingConstants.WS_ADDRESSING_VERSION,
				AddressingConstants.Submission.WSA_NAMESPACE);
		genericStub._getServiceClient().getOptions().setSoapVersionURI(SOAP11Constants.SOAP_ENVELOPE_NAMESPACE_URI);

	}

	// ------------------------------------------------------------------------------------------------------------------
	// Creazione del Project (Fascicolo)
	// ------------------------------------------------------------------------------------------------------------------
	private void createProject(String codeNodeClassification, String projectDescription) throws Exception {

		try {

			createProjectInner(codeNodeClassification, projectDescription);

			ProjectsStub wws = new ProjectsStub(wsURL);
			setServiceClientOptions(wws);

			CreateProjectRequest createProjectRequest = new CreateProjectRequest();
			createProjectRequest.setCodeAdm(codeAdm);
			createProjectRequest.setUserName(userName);
			createProjectRequest.setCodeRoleLogin(codeRoleLogin);
			// createProjectRequest.setCodeApplication(codeApplication);
			createProjectRequest.setAuthenticationToken(getAuthenticationTokenString);

			createProjectRequest.setProject(project);

			CreateProject createProject = new CreateProject();
			createProject.setRequest(createProjectRequest);
			CreateProjectResponse createProjectResponseDocument = new CreateProjectResponse();

			createProjectResponseDocument = wws.createProject(createProject);

			project = createProjectResponseDocument.getCreateProjectResult().getProject();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @param file
	 * @param codeNodeClassification
	 * @param projectDescription
	 * @param documentDescription
	 * @param documentObj
	 * @param recipientIDArray
	 * @param recipientTypeIDArray
	 * @param documentType
	 * @throws Exception
	 */
	private void createDocument(String fileName, String file, String codeNodeClassification, String projectDescription,
			String documentDescription, String documentObj, String[] recipientIDArray, String[] recipientTypeIDArray,
			String documentType, String senderName, String senderSurname, String senderCf, String senderEmail)
			throws Exception {

		try {

			createDocumentInner(documentDescription, documentObj, recipientIDArray, recipientTypeIDArray, fileName,
					file, documentType, senderName, senderSurname, senderCf, senderEmail);

			DocumentsStub wws = new DocumentsStub(wsURL);
			setServiceClientOptions(wws);

			CreateDocumentRequest createDocumentRequest = new CreateDocumentRequest();
			createDocumentRequest.setCodeAdm(codeAdm);
			createDocumentRequest.setUserName(userName);
			createDocumentRequest.setCodeRoleLogin(codeRoleLogin);
			createDocumentRequest.setCodeRegister(codeRegister);
			createDocumentRequest.setAuthenticationToken(getAuthenticationTokenString);

			// Fine creazione documento
			createDocumentRequest.setDocument(document);
			CreateDocument createDocument = new CreateDocument();
			createDocument.setRequest(createDocumentRequest);
			CreateDocumentResponse createDocumentResponse = new CreateDocumentResponse();

			createDocumentResponse = wws.createDocument(createDocument);

			document = createDocumentResponse.getCreateDocumentResult().getDocument();

			/*
			 * if (document.getSignature() != null) { jObInner.put("n_prot",
			 * document.getSignature()); } else { jObInner.put("n_prot", "");
			 *
			 * } jObInner.put("id_doc", document.getDocNumber());
			 *
			 * //documento principale if (document.getMainDocument() != null) {
			 * jObInner.put("main_doc_id", document.getMainDocument().getId());
			 * jObInner.put("main_doc_description",
			 * document.getMainDocument().getDescription());
			 * jObInner.put("main_doc_version", document.getMainDocument().getVersionId());
			 * jObInner.put("main_doc_mime_type", document.getMainDocument().getMimeType());
			 * jObInner.put("main_doc_name", document.getMainDocument().getName());
			 *
			 * //se non ho richiesto il file il content � null if
			 * (document.getMainDocument().getContent() != null) { InputStream in =
			 * document.getMainDocument().getContent().getInputStream(); byte[] byteArray =
			 * org.apache.commons.io.IOUtils.toByteArray(in); byte[] bytes64bytes =
			 * Base64.getEncoder().encode(byteArray); String content = new
			 * String(bytes64bytes);
			 *
			 * jObInner.put("main_doc_content", content); } }
			 */

		} catch (Exception e) {
			throw e;
		}

	}

	private void createProjectInner(String codeNodeClassification, String projectDescription) {

		try {

			project = new Project();
			project.setClassificationScheme(getActiveClassificationScheme()); // titolario attivo
			project.setDescription(projectDescription); // obbligatoria
			// project.setType("P"); //i P appaiono sulla "scrivania" di pitre e devono
			// essere accettati, solo all'accettazione si genera il numero di protocollo
			project.setCodeNodeClassification(codeNodeClassification); // Nodo del titolario nel quale creare il
																		// fascicolo

			/*
			 * ArrayOfNote note = new ArrayOfNote(); Note nota = new Note();
			 *
			 * nota.setDescription("Testo Nota"); Note[] arrayNota = new Note[1];
			 * arrayNota[0]= nota; note.setNote(arrayNota); project.setNote(note);
			 *
			 * //Associo tipologia fascicolo Template template = new Template();
			 * template.setName("NomeDelTemplate"); template.setId("IdDelTemplate");
			 * ArrayOfField fields = new ArrayOfField();
			 *
			 * Field[] fieldArray = new Field[1]; Field temp = new Field();
			 * temp.setName("NomeCampoProfilato"); temp.setValue("valoreCampoProfilato");
			 * fieldArray[0] = temp; fields.setField(fieldArray);
			 * template.setFields(fields); project.setTemplate(template);
			 */

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	// ------------------------------------------------------------------------------------------------------------------
	// createDocumentInner
	// ------------------------------------------------------------------------------------------------------------------
	private void createDocumentInner(String documentDescription, String documentObj, String[] recipientIDArray,
			String[] recipientTypeIDArray, String fileName, String file, String documentType, String senderName,
			String senderSurname, String senderCf, String senderEmail) throws Exception {

		try {

			// Creazione documento
			document = new Document();
			document.setObject(documentObj);

			/*
			 * Tipologia del documento. Può assumere i seguenti valori: A - protocollo o
			 * predisposto in arrivo. P - protocollo o predisposto in partenza. I -
			 * protocollo o predisposto interno. G - documento non protocollato.
			 */

			// document.setDocumentType("A");
			// document.setDocumentType("P");
			// Imposto il tipo di documento
			document.setDocumentType(documentType);

			// document.setPredisposed(false); //cablato che si tratta di un predisposto
			// //settandolo a false torna la signature (n protocollo) in automatico
			document.setPredisposed(true);

			ArrayOfNote note = new ArrayOfNote();
			Note nota = new Note();
			nota.setDescription(documentDescription);
			Note[] arrayNota = new Note[1];
			arrayNota[0] = nota;
			note.setNote(arrayNota);
			document.setNote(note);

			// Mittente censito nel sistema
			if (documentType.equals("A")) {
				document.setSender(getOccasionalCorrespondent(senderName, senderSurname, senderCf, senderEmail));
				document.setMeansOfSending("SERVIZI ONLINE");
			} else {
				// document.setSender(getSender());
				// document.setSender(getOccasionalCorrespondent(senderName, senderSurname,
				// senderCf, senderEmail));

				// Aggiungo un destinatario
				Correspondent receiver = getOccasionalCorrespondent(senderName, senderSurname, senderCf, senderEmail);
				ArrayOfCorrespondent recipients = new ArrayOfCorrespondent();
				/*
				 * Correspondent[] correspondentArray = new
				 * Correspondent[recipientIDArray.length]; for (int i = 0; i <
				 * recipientIDArray.length; i++) { Correspondent recipient = new
				 * Correspondent(); recipient.setId(recipientIDArray[i]); //per ora cablato un
				 * ruolo (P = utente, U = ufficio, R = ruolo)
				 * recipient.setCorrespondentType(recipientTypeIDArray[i]);
				 *
				 * correspondentArray[i] = recipient; }
				 */

				Correspondent[] correspondentArray = new Correspondent[1];
				correspondentArray[0] = receiver;
				recipients.setCorrespondent(correspondentArray);
				document.setRecipients(recipients);

			}

			// Inserisco File
			if (file != null) {

				// File file = new File(filePath);
				// byte fileContent[] = new byte[(int) file.length()];

				/*
				 * FileInputStream fin = new FileInputStream(file); int read = 0;
				 * ByteArrayOutputStream ous = new ByteArrayOutputStream(); while ((read =
				 * fin.read(fileContent)) != -1) { ous.write(fileContent, 0, read); }
				 * fin.close();
				 */

				byte fileContent[] = Base64.getDecoder().decode(file);

				it.opencontent.nttdata._2012.pi3.File mainDocument = new it.opencontent.nttdata._2012.pi3.File();

				org.apache.axiom.attachments.ByteArrayDataSource rawData = new org.apache.axiom.attachments.ByteArrayDataSource(
						fileContent);
				DataHandler data = new DataHandler(rawData);

				String fixedFileName = P3Utils.getCustomMimeType(fileName);
				String contentType = MimeTypeResolver.findMimeType(fileName);

				mainDocument.setName(fixedFileName);
				mainDocument.setMimeType(contentType);
				mainDocument.setContent(data);

				document.setMainDocument(mainDocument);
			}

			/*
			 * //Associo tipologia documento Template template = new Template();
			 * template.setName("Nome tipologia Template"); template.setId("IdTemplate");
			 *
			 * ArrayOfField fields = new ArrayOfField(); Field[] fieldArray = new Field[2];
			 * Field temp = new Field(); temp.setName("NomeCampoProfilato");
			 * temp.setValue("Valore"); fieldArray[0] = temp;
			 *
			 * temp = new Field(); temp.setName("NomeCampoProfilato");
			 * temp.setValue("Valore");
			 *
			 * fieldArray[1] = temp; temp = new Field(); fields.setField(fieldArray);
			 * template.setFields(fields); document.setTemplate(template);
			 */

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	// ------------------------------------------------------------------------------------------------------------------
	// getSenderId
	// ------------------------------------------------------------------------------------------------------------------
	private Correspondent getSender() {

		if (sender == null) {
			Correspondent senderInner = new Correspondent();
			senderInner.setCode(userName);
			senderInner.setCorrespondentType("P"); // Tipologia di corrispondente - può assumere i seguenti valori (P =
													// Persona, U = Ufficio, R = Ruolo)
			senderInner.setType("I"); // Tipo di corrispondente - può assumere i seguenti valori (I = Interno, E =
										// Esterno, O = Occasionale)
			senderInner.setId(userID); // non dovrebbe servire, ma se non si imposta non funziona l'aggiunta del file
										// nel mainDocument

			sender = senderInner;
		}

		return sender;
	}

	private Correspondent getOccasionalCorrespondent(String name, String surname, String cf, String email) {
		Correspondent sender = new Correspondent();
		sender.setCorrespondentType("P"); // Può assumere i seguenti valori (P = Persona, U = Ufficio, R = Ruolo)
		sender.setType("O"); // Tipo di corrispondente - può assumere i seguenti valori (I = Interno, E =
								// Esterno, O = Occasionale)

		sender.setName(name);
		sender.setSurname(surname);
		sender.setNationalIdentificationNumber(cf);
		sender.setEmail(email);
		sender.setDescription(name + " " + surname + " " + cf + " - " + email);

		return sender;
	}

	// ------------------------------------------------------------------------------------------------------------------
	// getCorrispondentFilters
	// ------------------------------------------------------------------------------------------------------------------
	public ArrayOfCorrespondent searchUser(String code) {

		ArrayOfCorrespondent result = new ArrayOfCorrespondent();

		try {
			getAuthenticationToken();

			AddressBookStub wws = new AddressBookStub(wsURL);
			setServiceClientOptions(wws);

			SearchCorrespondentsRequest searchCorrespondentsRequest = new SearchCorrespondentsRequest();
			searchCorrespondentsRequest.setCodeAdm(codeAdm);
			searchCorrespondentsRequest.setUserName(userName);

			int length = 4;
			if (code != null) {
				length = 5;
			}
			Filter[] filterArray = new Filter[5];
			ArrayOfFilter filters = new ArrayOfFilter();
			Filter filter = new Filter();
			filter.setName("OFFICES");
			filter.setValue("True");
			filter.setType(FilterType.Bool);
			filterArray[0] = filter;
			filter = new Filter();
			filter.setName("USERS");
			filter.setValue("True");
			filter.setType(FilterType.Bool);
			filterArray[1] = filter;
			filter = new Filter();
			filter.setName("ROLES");
			filter.setValue("True");
			filter.setType(FilterType.Bool);
			filterArray[2] = filter;
			filter = new Filter();
			filter.setName("TYPE");
			filter.setValue("GLOBAL");
			filter.setType(FilterType.String);
			filterArray[3] = filter;

			if (code != null) {
				filter = new Filter();
				filter.setName("EXACT_CODE");
				filter.setValue(code);
				filter.setType(FilterType.String);
				filterArray[4] = filter;

				/*
				 * filter = new Filter(); filter.setName("NATIONAL_IDENTIFICATION_NUMBER");
				 * filter.setValue(code); filter.setType(FilterType.String); filterArray[4] =
				 * filter;
				 */
			}
			filters.setFilter(filterArray);
			searchCorrespondentsRequest.setFilters(filters);

			SearchCorrespondents searchCorrespondents = new SearchCorrespondents();
			searchCorrespondents.setRequest(searchCorrespondentsRequest);

			SearchCorrespondentsResponse searchCorrespondentsResponse = wws.searchCorrespondents(searchCorrespondents);

			// searchCorrespondentsResponse =
			// wws.searchCorrespondents(searchCorrespondents);
			result = searchCorrespondentsResponse.getSearchCorrespondentsResult().getCorrespondents();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * @param file
	 * @param codeNodeClassification
	 * @param projectDescription
	 * @param documentDescription
	 * @param documentObj
	 * @param recipientIDArray
	 * @param recipientTypeIDArray
	 * @param trasmissionIDArray
	 * @param createProject
	 * @param idProject
	 * @param documentType
	 * @param senderName
	 * @param senderSurname
	 * @param senderCf
	 * @param senderEmail
	 * @return
	 */
	public JSONObject createDocumentAndAddInProject(String fileName, String file, String codeNodeClassification,
			String projectDescription, String documentDescription, String documentObj, String[] recipientIDArray,
			String[] recipientTypeIDArray, String[] trasmissionIDArray, boolean createProject, String idProject,
			String documentType, String senderName, String senderSurname, String senderCf, String senderEmail) {

		JSONObject jObjd = new JSONObject();
		JSONObject jObInner = new JSONObject();

		try {
			getAuthenticationToken();

			jObjd.put("data", jObInner);

			if (codeNodeClassification == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "codeNodeClassification è un valore obbligatorio");
				return jObjd;
			}

			if (projectDescription == null && idProject == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "projectDescription è un valore obbligatorio");
				return jObjd;
			}

			if (documentDescription == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "documentDescription è un valore obbligatorio");
				return jObjd;
			}

			if (documentObj == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "documentObj è un valore obbligatorio");
				return jObjd;
			}

			if (recipientIDArray == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "recipientIDArray è un valore obbligatorio");
				return jObjd;
			}

			if (recipientTypeIDArray == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "recipientTypeIDArray è un valore obbligatorio");
				return jObjd;
			}

			if (trasmissionIDArray == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "trasmissionIDArray è un valore obbligatorio");
				return jObjd;
			}

			if (createProject && idProject != null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message",
						"Non è consentito valorizzare createProject a true e idProject contemporaneamente. Il primo crea un nuovo progetto, il secondo aggiunge il documento ad un progetto esistente");
				return jObjd;
			}

			if (recipientIDArray.length != recipientTypeIDArray.length) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "recipientIDArray e recipientTypeIDArray non hanno lo stesso numero di elementi");
				return jObjd;
			}

			if (documentType == null) {

				/*
				 * jObjd.put("status", P3Utils.LEVEL_ERROR); jObjd.put("message",
				 * "documentType è un valore obbligatorio"); return jObjd;
				 */
				// Non è stato passato il document type, lo imposto per default a documento in
				// arrivo (A)
				documentType = "A";
			}

			// creazione solo del documento, senza progetto
			if (!createProject && idProject == null) {

				createDocument(fileName, file, codeNodeClassification, projectDescription, documentDescription,
						documentObj, recipientIDArray, recipientTypeIDArray, documentType, senderName, senderSurname,
						senderCf, senderEmail);

				// creazione del documento, creazione del progetto o aggiunta a progetto
				// esistente
			} else {

				// creazione nuovo project
				if (createProject) {
					createProject(codeNodeClassification, projectDescription);
					idProject = project.getId();
				}

				// preparo l'oggetto documento
				createDocumentInner(documentDescription, documentObj, recipientIDArray, recipientTypeIDArray, fileName,
						file, documentType, senderName, senderSurname, senderCf, senderEmail);

				// creo tutto
				// -----------------------------------------------------------------------------------------------------------------
				DocumentsStub wws = new DocumentsStub(wsURL);
				setServiceClientOptions(wws);

				CreateDocumentAndAddInProjectRequest createDocumentAndAddInProjectRequest = new CreateDocumentAndAddInProjectRequest();
				createDocumentAndAddInProjectRequest.setCodeAdm(codeAdm);
				createDocumentAndAddInProjectRequest.setUserName(userName);
				createDocumentAndAddInProjectRequest.setCodeRoleLogin(codeRoleLogin);
				createDocumentAndAddInProjectRequest.setAuthenticationToken(getAuthenticationTokenString);

				createDocumentAndAddInProjectRequest.setDocument(document);
				createDocumentAndAddInProjectRequest.setCodeRegister(codeRegister);
				// createDocumentAndAddInProjectRequest.setCodeRF("CodeRF"); //CodeRF serve solo
				// nel caso di un protocollo, e solo se non è stato inserito il CodeRegister (in
				// questo caso mi pare REG_CCT)

				createDocumentAndAddInProjectRequest.setIdProject(idProject);
				// createDocumentAndAddInProjectRequest.setCodeProject(project.getCode());
				// createDocumentAndAddInProjectRequest.setClassificationSchemeId(codeNodeClassification);

				CreateDocumentAndAddInProject createDocumentAndAddInProject = new CreateDocumentAndAddInProject();
				createDocumentAndAddInProject.setRequest(createDocumentAndAddInProjectRequest);

				CreateDocumentAndAddInProjectResponse createDocumentAndAddInProjectResponse = new CreateDocumentAndAddInProjectResponse();

				createDocumentAndAddInProjectResponse = wws
						.createDocumentAndAddInProject(createDocumentAndAddInProject);

				document = createDocumentAndAddInProjectResponse.getCreateDocumentAndAddInProjectResult().getDocument();
				// -----------------------------------------------------------------------------------------------------------------

				// eseguo la trasmissione
				if (documentType.equals("A")) {
					executeTransmissionDocument(trasmissionIDArray, null);
				}

			}

			// invoco la getLastFileId per ricercare l'id del documento appena creato
			String id_file = getLastFileId(document.getDocNumber());

			// gestione messaggio di ritorno
			jObInner.put("n_prot", document.getSignature());
			jObInner.put("id_doc", document.getDocNumber());
			jObInner.put("id_file", id_file);
			jObInner.put("id_proj", idProject);

			jObjd.put("status", P3Utils.LEVEL_SUCCESS);
			jObjd.put("message", P3Utils.DEFAULT_SUCCESS_MESSAGE);

		} catch (Exception e) {
			try {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", e.getMessage());
			} catch (JSONException e1) {
			}

			e.printStackTrace();
		}

		return jObjd;

	}

	public JSONObject createDocumentPredisposed(String fileName, String file, String codeNodeClassification,
			String projectDescription, String documentDescription, String documentObj, String[] recipientIDArray,
			String[] recipientTypeIDArray, String[] trasmissionIDArray, boolean createProject, String idProject,
			String documentType, String senderName, String senderSurname, String senderCf, String senderEmail) {

		JSONObject jObjd = new JSONObject();
		JSONObject jObInner = new JSONObject();

		try {
			getAuthenticationToken();

			jObjd.put("data", jObInner);

			if (codeNodeClassification == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "codeNodeClassification è un valore obbligatorio");
				return jObjd;
			}

			if (projectDescription == null && idProject == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "projectDescription è un valore obbligatorio");
				return jObjd;
			}

			if (documentDescription == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "documentDescription è un valore obbligatorio");
				return jObjd;
			}

			if (documentObj == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "documentObj è un valore obbligatorio");
				return jObjd;
			}

			if (recipientIDArray == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "recipientIDArray è un valore obbligatorio");
				return jObjd;
			}

			if (recipientTypeIDArray == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "recipientTypeIDArray è un valore obbligatorio");
				return jObjd;
			}

			if (trasmissionIDArray == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "trasmissionIDArray è un valore obbligatorio");
				return jObjd;
			}

			if (createProject && idProject != null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message",
						"Non è consentito valorizzare createProject a true e idProject contemporaneamente. Il primo crea un nuovo progetto, il secondo aggiunge il documento ad un progetto esistente");
				return jObjd;
			}

			if (recipientIDArray.length != recipientTypeIDArray.length) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "recipientIDArray e recipientTypeIDArray non hanno lo stesso numero di elementi");
				return jObjd;
			}

			if (documentType == null) {
				// Non è stato passato il document type, lo imposto per default a documento in
				// arrivo (A)
				documentType = "A";
			}

			// creazione nuovo project
			if (createProject) {
				createProject(codeNodeClassification, projectDescription);
				idProject = project.getId();
				System.out.println("Questo è l'id del mio progetto");
				System.out.println(idProject);
			} else if (idProject != null) {
				JSONObject result = getProject(idProject);

				System.out.println("Stampo il risultato del getProject");
				System.out.println(result.toString());
			}

			// preparo l'oggetto documento
			// createDocumentInner(documentDescription, documentObj, recipientIDArray,
			// recipientTypeIDArray, fileName, file, documentType, senderName,
			// senderSurname, senderCf, senderEmail);
			createDocument(fileName, file, codeNodeClassification, projectDescription, documentDescription, documentObj,
					recipientIDArray, recipientTypeIDArray, documentType, senderName, senderSurname, senderCf,
					senderEmail);

			addDocumentInProject();

			// invoco la getLastFileId per ricercare l'id del documento appena creato
			String id_file = getLastFileId(document.getDocNumber());

			// gestione messaggio di ritorno
			jObInner.put("n_prot", document.getSignature());
			jObInner.put("id_doc", document.getDocNumber());
			jObInner.put("id_file", id_file);
			jObInner.put("id_proj", idProject);

			jObjd.put("status", P3Utils.LEVEL_SUCCESS);
			jObjd.put("message", P3Utils.DEFAULT_SUCCESS_MESSAGE);

		} catch (Exception e) {
			try {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", e.getMessage());
			} catch (JSONException e1) {
			}

			e.printStackTrace();
		}

		return jObjd;

	}


	/**
	 * @param documentId
	 * @throws Exception
	 */
	public JSONObject protocolPredisposed(String documentId, String[] trasmissionIDArray) throws Exception {

		JSONObject jObjd = new JSONObject();
		try {

			jObjd = getDocument(documentId, false);

			if (document instanceof Document) {
				getAuthenticationToken();

				DocumentsStub wws = new DocumentsStub(wsURL);
				setServiceClientOptions(wws);

				EditDocumentRequest editDocumentRequest = new EditDocumentRequest();
				editDocumentRequest.setCodeAdm(codeAdm);
				editDocumentRequest.setUserName(userName);
				editDocumentRequest.setCodeRoleLogin(codeRoleLogin);
				editDocumentRequest.setAuthenticationToken(getAuthenticationTokenString);

				document.setPredisposed(false);
				editDocumentRequest.setDocument(document);

				EditDocument editDocument = new EditDocument();
				editDocument.setRequest(editDocumentRequest);


				EditDocumentResponse editDocumentResponse = new EditDocumentResponse();
				editDocumentResponse = wws.editDocument(editDocument);

				document = editDocumentResponse.getEditDocumentResult().getDocument();

				System.out.println("Stampo il risultato della protocollazione del predisposto");
				System.out.println(document.getSignature());
				jObjd = documentToJson();

				if (document.getDocumentType().equals("A")) {
					executeTransmissionDocument(trasmissionIDArray, null);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return jObjd;
	}



	public void addDocumentInProject() throws Exception {

		DocumentsStub wws = new DocumentsStub(wsURL);
		setServiceClientOptions(wws);

		AddDocInProjectRequest addDocInProjectRequest = new AddDocInProjectRequest();
		addDocInProjectRequest.setCodeAdm(codeAdm);
		addDocInProjectRequest.setUserName(userName);
		addDocInProjectRequest.setAuthenticationToken(getAuthenticationTokenString);

		addDocInProjectRequest.setIdDocument(document.getId());
		addDocInProjectRequest.setIdProject(project.getId());

		// AddDocInProjectDocument addDocInProjectDocument =
		// AddDocInProjectDocument.Factory.newInstance();

		AddDocInProject addDocInProject = new AddDocInProject();
		addDocInProject.setRequest(addDocInProjectRequest);
		// addDocInProjectDocument.setAddDocInProject(addDocInProject);
		// AddDocInProjectResponseDocument addDocInProjectResponseDocument =
		// AddDocInProjectResponseDocument.Factory.newInstance();

		AddDocInProjectResponse addDocInProjectResponse = new AddDocInProjectResponse();
		addDocInProjectResponse = wws.addDocInProject(addDocInProject);


	}

	// ------------------------------------------------------------------------------------------------------------------
	// uploadFileToDocument
	// ------------------------------------------------------------------------------------------------------------------
	public JSONObject uploadFileToDocument(String documentId, String fileName, String file,
			String attachmentDescription, boolean createAttachment, String attachmentType) {

		JSONObject jObjd = new JSONObject();
		JSONObject jObInner = new JSONObject();

		try {
			jObjd.put("data", jObInner);

			if (documentId == null || file == null || attachmentDescription == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "documentId, filePath e attachmentDescription sono valori obbligatori");
				return jObjd;
			}

			getAuthenticationToken();

			DocumentsStub wws = new DocumentsStub(wsURL);
			setServiceClientOptions(wws);

			UploadFileToDocumentRequest uploadFileToDocumentRequest = new UploadFileToDocumentRequest();

			uploadFileToDocumentRequest.setCodeAdm(codeAdm);
			uploadFileToDocumentRequest.setUserName(userName);
			uploadFileToDocumentRequest.setCodeRoleLogin(codeRoleLogin);
			uploadFileToDocumentRequest.setAuthenticationToken(getAuthenticationTokenString);
			uploadFileToDocumentRequest.setIdDocument(documentId);
			uploadFileToDocumentRequest.setCreateAttachment(createAttachment); // Se true crea un nuovo allegato (� true
																				// di default)
			uploadFileToDocumentRequest.setAttachmentType(attachmentType); // se non passato viene settato a U = utente
																			// (altro valore accettato � E = esterno)
			uploadFileToDocumentRequest.setDescription(attachmentDescription);

			// Inserisco File
			byte fileContent[] = Base64.getDecoder().decode(file);

			it.opencontent.nttdata._2012.pi3.File mainDocument = new it.opencontent.nttdata._2012.pi3.File();

			org.apache.axiom.attachments.ByteArrayDataSource rawData = new org.apache.axiom.attachments.ByteArrayDataSource(
					fileContent);
			DataHandler data = new DataHandler(rawData);

			String fixedFileName = P3Utils.getCustomMimeType(fileName);
			String contentType = MimeTypeResolver.findMimeType(fileName);

			mainDocument.setName(fixedFileName);
			mainDocument.setMimeType(contentType);
			mainDocument.setContent(data);

			uploadFileToDocumentRequest.setFile(mainDocument);

			UploadFileToDocument uploadFileToDocument = new UploadFileToDocument();
			uploadFileToDocument.setRequest(uploadFileToDocumentRequest);
			// UploadFileToDocumentResponse uploadFileToDocumentResponse = new
			// UploadFileToDocumentResponse();

			wws.uploadFileToDocument(uploadFileToDocument);

			// invoco la getLastFileId per ricercare l'id del documento appena creato
			String id_file = getLastFileId(documentId);

			// gestione messaggio di ritorno
			jObInner.put("id_doc", documentId);
			jObInner.put("id_file", id_file);
			jObjd.put("status", P3Utils.LEVEL_SUCCESS);
			jObjd.put("message", P3Utils.DEFAULT_SUCCESS_MESSAGE);

		} catch (Exception e) {
			try {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", e.getMessage());
			} catch (JSONException e1) {
			}

			e.printStackTrace();
		}

		return jObjd;

	}

	// ------------------------------------------------------------------------------------------------------------------
	//
	// ------------------------------------------------------------------------------------------------------------------
	private String getLastFileId(String documentId) {

		JSONObject jObjd = new JSONObject();
		JSONObject jObInner = new JSONObject();

		String docId = null;

		try {
			jObjd = getDocument(documentId, false);

			if (jObInner != null) {

				jObInner = (JSONObject) jObjd.get("data");
				docId = jObInner.get("main_doc_id").toString();

				if (jObInner.has("attachments")) {

					JSONArray jObjdAttachCollection = (JSONArray) jObInner.get("attachments");

					if (jObjdAttachCollection != null && jObjdAttachCollection.length() != 0) {
						jObInner = jObjdAttachCollection.getJSONObject(jObjdAttachCollection.length() - 1);

						if (jObInner != null) {
							docId = jObInner.get("attach_doc_id").toString();
						}
					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return docId;
	}

	// ------------------------------------------------------------------------------------------------------------------
	//
	// ------------------------------------------------------------------------------------------------------------------
	public JSONObject getDocument(String documentId, boolean getFile) {

		JSONObject jObjd = new JSONObject();
		JSONObject jObInner = new JSONObject();

		try {

			jObjd.put("data", jObInner);

			if (documentId == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "documentId � un valore obbligatorio");
				return jObjd;
			}

			getAuthenticationToken();

			DocumentsStub wws = new DocumentsStub(wsURL);
			setServiceClientOptions(wws);

			GetDocumentRequest getDocumentRequest = new GetDocumentRequest();
			getDocumentRequest.setCodeAdm(codeAdm);
			getDocumentRequest.setUserName(userName);
			getDocumentRequest.setCodeRoleLogin(codeRoleLogin);
			getDocumentRequest.setAuthenticationToken(getAuthenticationTokenString);

			getDocumentRequest.setIdDocument(documentId);
			getDocumentRequest.setGetFile(getFile);
			// getDocumentRequest.setSignature("Segnatura"); opzionale al posto dell'id

			GetDocument getDocument = new GetDocument();
			getDocument.setRequest(getDocumentRequest);
			GetDocumentResponse getDocumentResponse = new GetDocumentResponse();

			getDocumentResponse = wws.getDocument(getDocument);

			document = getDocumentResponse.getGetDocumentResult().getDocument();

			// gestione messaggio di ritorno

			if (document.getSignature() != null) {
				jObInner.put("n_prot", document.getSignature());
			} else {
				jObInner.put("n_prot", "");

			}
			jObInner.put("id_doc", document.getDocNumber());

			// documento principale
			if (document.getMainDocument() != null) {
				jObInner.put("main_doc_id", document.getMainDocument().getId());
				jObInner.put("main_doc_description", document.getMainDocument().getDescription());
				jObInner.put("main_doc_version", document.getMainDocument().getVersionId());
				jObInner.put("main_doc_mime_type", document.getMainDocument().getMimeType());
				jObInner.put("main_doc_name", document.getMainDocument().getName());

				// se non ho richiesto il file il content � null
				if (document.getMainDocument().getContent() != null) {
					InputStream in = document.getMainDocument().getContent().getInputStream();
					byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);
					byte[] bytes64bytes = Base64.getEncoder().encode(byteArray);
					String content = new String(bytes64bytes);

					jObInner.put("main_doc_content", content);
				}
			}

			// documento principale
			if (document.getAttachments() != null) {

				it.opencontent.nttdata._2012.pi3.File[] attachments = document.getAttachments().getFile();

				Collection<JSONObject> jObjdAttachCollection = new ArrayList<JSONObject>();

				for (int i = 0; i < attachments.length; i++) {

					JSONObject jObjdAttach = new JSONObject();

					jObjdAttach.put("attach_doc_id", attachments[i].getId());
					jObjdAttach.put("attach_doc_description", attachments[i].getDescription());
					jObjdAttach.put("attach_doc_version", attachments[i].getVersionId());
					jObjdAttach.put("attach_doc_mime_type", attachments[i].getMimeType());
					jObjdAttach.put("attach_doc_name", attachments[i].getName());

					// se non ho richiesto il file il content � null
					if (attachments[i].getContent() != null) {
						InputStream in = attachments[i].getContent().getInputStream();
						byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);
						byte[] bytes64bytes = Base64.getEncoder().encode(byteArray);
						String content = new String(bytes64bytes);

						jObjdAttach.put("attach_doc_content", content);
					}

					jObjdAttachCollection.add(jObjdAttach);

				}

				jObInner.put("attachments", jObjdAttachCollection);

			}

			jObjd.put("status", P3Utils.LEVEL_SUCCESS);
			jObjd.put("message", P3Utils.DEFAULT_SUCCESS_MESSAGE);

		} catch (Exception e) {
			try {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", e.getMessage());
			} catch (JSONException e1) {
			}

			e.printStackTrace();
		}

		return jObjd;

	}

	public JSONObject documentToJson() {

		JSONObject jObjd = new JSONObject();
		JSONObject jObInner = new JSONObject();

		try {

			if (document == null) {
				throw new Exception("L'attributo document non è stato inizializzato, eseguire almeno un getDocumnt");
			}

			jObjd.put("data", jObInner);

			if (document.getSignature() != null) {
				jObInner.put("n_prot", document.getSignature());
			} else {
				jObInner.put("n_prot", "");

			}
			jObInner.put("id_doc", document.getDocNumber());

			// documento principale
			if (document.getMainDocument() != null) {
				jObInner.put("main_doc_id", document.getMainDocument().getId());
				jObInner.put("main_doc_description", document.getMainDocument().getDescription());
				jObInner.put("main_doc_version", document.getMainDocument().getVersionId());
				jObInner.put("main_doc_mime_type", document.getMainDocument().getMimeType());
				jObInner.put("main_doc_name", document.getMainDocument().getName());

				// se non ho richiesto il file il content � null
				if (document.getMainDocument().getContent() != null) {
					InputStream in = document.getMainDocument().getContent().getInputStream();
					byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);
					byte[] bytes64bytes = Base64.getEncoder().encode(byteArray);
					String content = new String(bytes64bytes);

					jObInner.put("main_doc_content", content);
				}
			}

			// documento principale
			if (document.getAttachments() != null) {

				it.opencontent.nttdata._2012.pi3.File[] attachments = document.getAttachments().getFile();

				Collection<JSONObject> jObjdAttachCollection = new ArrayList<JSONObject>();

				for (int i = 0; i < attachments.length; i++) {

					JSONObject jObjdAttach = new JSONObject();

					jObjdAttach.put("attach_doc_id", attachments[i].getId());
					jObjdAttach.put("attach_doc_description", attachments[i].getDescription());
					jObjdAttach.put("attach_doc_version", attachments[i].getVersionId());
					jObjdAttach.put("attach_doc_mime_type", attachments[i].getMimeType());
					jObjdAttach.put("attach_doc_name", attachments[i].getName());

					// se non ho richiesto il file il content � null
					if (attachments[i].getContent() != null) {
						InputStream in = attachments[i].getContent().getInputStream();
						byte[] byteArray = org.apache.commons.io.IOUtils.toByteArray(in);
						byte[] bytes64bytes = Base64.getEncoder().encode(byteArray);
						String content = new String(bytes64bytes);

						jObjdAttach.put("attach_doc_content", content);
					}

					jObjdAttachCollection.add(jObjdAttach);

				}

				jObInner.put("attachments", jObjdAttachCollection);

			}

			jObjd.put("status", P3Utils.LEVEL_SUCCESS);
			jObjd.put("message", P3Utils.DEFAULT_SUCCESS_MESSAGE);

		} catch (Exception e) {
			try {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", e.getMessage());
			} catch (JSONException e1) {
			}

			e.printStackTrace();
		}

		return jObjd;

	}


	public JSONObject getProject(String idProject) {

		JSONObject jObjd = new JSONObject();
		JSONObject jObInner = new JSONObject();

		try {

			jObjd.put("data", jObInner);

			if (idProject == null) {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", "idProject � un valore obbligatorio");
				return jObjd;
			}

			getAuthenticationToken();

			ProjectsStub wws = new ProjectsStub(wsURL);

			setServiceClientOptions(wws);

			GetProjectRequest getProjectRequest = new GetProjectRequest();

			getProjectRequest.setCodeAdm(codeAdm);
			getProjectRequest.setUserName(userName);
			getProjectRequest.setIdProject(idProject);

			GetProject getProject = new GetProject();
			getProject.setRequest(getProjectRequest);

			GetProjectResponse getProjectResponseDocument = wws.getProject(getProject);
			project = getProjectResponseDocument.getGetProjectResult().getProject();

			jObInner.put("id_proj", project.getId());
			jObInner.put("n_proj", project.getNumber());
			jObInner.put("code_proj", project.getCode());

			jObjd.put("status", P3Utils.LEVEL_SUCCESS);
			jObjd.put("message", P3Utils.DEFAULT_SUCCESS_MESSAGE);
		} catch (Exception e) {
			try {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", e.getMessage());
			} catch (JSONException e1) {
			}

			e.printStackTrace();
		}

		return jObjd;
	}

	public JSONObject getProjectByDocument(String documentId) {

		JSONObject jObjd = new JSONObject();
		JSONObject jObInner = new JSONObject();

		try {

			getAuthenticationToken();

			ProjectsStub wws = new ProjectsStub(wsURL);

			setServiceClientOptions(wws);

			GetProjectsByDocumentRequest getProjectsByDocumentRequest = new GetProjectsByDocumentRequest();


			getProjectsByDocumentRequest.setCodeAdm(codeAdm);
			getProjectsByDocumentRequest.setUserName(userName);
			getProjectsByDocumentRequest.setIdDocument(documentId);

			GetProjectsByDocument getProjectsByDocument = new GetProjectsByDocument();
			getProjectsByDocument.setRequest(getProjectsByDocumentRequest);

			GetProjectsByDocumentResponse getProjectsByDocumentResponse = wws.getProjectsByDocument(getProjectsByDocument);

			ArrayOfProject result = getProjectsByDocumentResponse.getGetProjectsByDocumentResult().getProjects();

			Project[] projects = result.getProject();

			for (int i = 0; i < projects.length; i++) {
				System.out.println("Projects " + projects[i].getId());
			}


	    } catch (Exception e) {
			try {
				jObjd.put("status", P3Utils.LEVEL_ERROR);
				jObjd.put("message", e.getMessage());
			} catch (JSONException e1) {
			}

			e.printStackTrace();
		}

		return jObjd;
	}

	// ------------------------------------------------------------------------------------------------------------------
	//
	// ------------------------------------------------------------------------------------------------------------------

	public void executeTransmissionDocument(String[] trasmissionIDArray, Document document) throws Exception {

		try {

			getAuthenticationToken();

			TransmissionsStub wws = new TransmissionsStub(wsURL);
			setServiceClientOptions(wws);

			ExecuteTransmissionDocumentRequest executeTransmissionDocumentRequest = new ExecuteTransmissionDocumentRequest();
			executeTransmissionDocumentRequest.setCodeAdm(codeAdm);
			executeTransmissionDocumentRequest.setUserName(userName);
			executeTransmissionDocumentRequest.setCodeRoleLogin(codeRoleLogin);
			executeTransmissionDocumentRequest.setAuthenticationToken(getAuthenticationTokenString);

			if (document == null) {
				document = this.document;
			}
			executeTransmissionDocumentRequest.setIdDocument(document.getId());
			executeTransmissionDocumentRequest.setTransmissionType("S");
			executeTransmissionDocumentRequest.setNotify(true);
			// Ragione ditrasmissione specifica le trasmissioni automatiche provenienti da
			// servizi esterni
			executeTransmissionDocumentRequest.setTransmissionReason("TRASM ONLINE");


			for (int i = 0; i < trasmissionIDArray.length; i++) {

				System.out.println("Eseguo trasmissione a:" + trasmissionIDArray[i]);
				Correspondent recipient = new Correspondent();
				recipient.setCode(trasmissionIDArray[i]);

				executeTransmissionDocumentRequest.setReceiver(recipient);

				ExecuteTransmissionDocument executeTransmissionDocument = new ExecuteTransmissionDocument();
				executeTransmissionDocument.setRequest(executeTransmissionDocumentRequest);
				ExecuteTransmissionDocumentResponse executeTransmissionDocumentResponse = new ExecuteTransmissionDocumentResponse();

				executeTransmissionDocumentResponse = wws.executeTransmissionDocument(executeTransmissionDocument);
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	public void executeTransmissionProject(String[] trasmissionIDArray, String projectId) throws Exception {

		  try {

			  getAuthenticationToken();

			  TransmissionsStub wws = new TransmissionsStub(wsURL);
			  setServiceClientOptions(wws);

			  ExecuteTransmissionProjectRequest executeTransmissionProjectRequest = new ExecuteTransmissionProjectRequest();
			  executeTransmissionProjectRequest.setCodeAdm(codeAdm);
			  executeTransmissionProjectRequest.setUserName(userName);
			  executeTransmissionProjectRequest.setCodeRoleLogin(codeRoleLogin);
			  executeTransmissionProjectRequest.setAuthenticationToken(getAuthenticationTokenString);

			  executeTransmissionProjectRequest.setIdProject(projectId);
			  executeTransmissionProjectRequest.setTransmissionType("S");
			  executeTransmissionProjectRequest.setNotify(true);
			  executeTransmissionProjectRequest.setTransmissionReason("TRASM ONLINE");

			  for (int i = 0; i < trasmissionIDArray.length; i++) {

				System.out.println("Eseguo trasmissione a:" + trasmissionIDArray[i]);
				Correspondent recipient = new Correspondent();
				recipient.setCode(trasmissionIDArray[i]);

				executeTransmissionProjectRequest.setReceiver(recipient);

				ExecuteTransmissionProject executeTransmissionProject = new ExecuteTransmissionProject();
				executeTransmissionProject.setRequest(executeTransmissionProjectRequest);

				ExecuteTransmissionProjectResponse executeTransmissionProjectResponse = new ExecuteTransmissionProjectResponse();

				executeTransmissionProjectResponse = wws.executeTransmissionProject(executeTransmissionProject);

			}

		  } catch(Exception e) {
			  e.printStackTrace();
				throw e;
		  }

	  }


	// ------------------------------------------------------------------------------------------------------------------
	// Recupera il titolario attivo
	// ------------------------------------------------------------------------------------------------------------------
	private ClassificationScheme getActiveClassificationScheme() {

		ClassificationScheme scheme = null;

		try {

			getAuthenticationToken();

			ClassificationSchemesStub wws = new ClassificationSchemesStub(wsURL);
			setServiceClientOptions(wws);

			GetActiveClassificationSchemeRequest getActiveClassificationSchemeRequest = new GetActiveClassificationSchemeRequest();

			getActiveClassificationSchemeRequest.setCodeAdm(codeAdm);
			getActiveClassificationSchemeRequest.setUserName(userName);
			// getActiveClassificationSchemeRequest.setCodeRoleLogin(codeRoleLogin);
			getActiveClassificationSchemeRequest.setAuthenticationToken(getAuthenticationTokenString);

			GetActiveClassificationScheme getActiveClassificationScheme = new GetActiveClassificationScheme();
			getActiveClassificationScheme.setRequest(getActiveClassificationSchemeRequest);

			GetActiveClassificationSchemeResponse getActiveClassificationSchemeResponse = new GetActiveClassificationSchemeResponse();

			getActiveClassificationSchemeResponse = wws.getActiveClassificationScheme(getActiveClassificationScheme);
			scheme = getActiveClassificationSchemeResponse.getGetActiveClassificationSchemeResult()
					.getClassificationScheme();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return scheme;
	}


}
